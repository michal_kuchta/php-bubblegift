<?php
namespace App\Admin\Composers;

use Illuminate\View\View;

class MenuComposer
{
    public function compose(View $view)
    {
        $menu = include_once base_path('app/Admin/menu.php');

        $view->with('items', $menu);
    }

}