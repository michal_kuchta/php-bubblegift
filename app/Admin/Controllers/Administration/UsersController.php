<?php

namespace App\Admin\Controllers\Administration;

use App\Extensions\Grid\Grid;
use App\Extensions\Rules\UserRules;
use App\Models\Core\User;
use App\Admin\Controllers\DefaultController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\View\View;

class UsersController extends DefaultController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        parent::__construct();

        $this->baseUrl .= '/administration/users';

        view()->composer('views.administration.users.*', function (View $view){
            $view->with('baseUrl', $this->baseUrl);
        });
    }

    public function getIndex()
    {
        $grid = new Grid(UsersController::class, 'index');

        $grid->setTitle(__('Użytkownicy'));

        $grid->setPagination();
        $grid->setCheckboxes();
        $filters = session()->get(self::class . '@index', []);
        $grid->setFilters(view('views.administration.users.filters', ['model' => ['filters' => $filters]])->render());

        $grid->addColumn('id', __('ID'), null, true);
        $grid->addColumn('email', __('E-mail'), null, true);
        $grid->addColumn('fullName', __('Imię i nazwisko'), null, false);

        $grid->addAction('edit', 'id', $this->baseUrl . '/edit/{id}');
        $grid->addAction('delete', 'deleteColumn', $this->baseUrl . '/delete/{id}');
        $grid->addIcon('edit', 'fas fa-edit', 'edit');
        $grid->addIcon('delete', 'fas fa-times', 'delete', '', 'btn-danger', ['data-confirm' => __('Jesteś pewien usunięcia użytkownika?')]);

        $grid->setData(User::getUsersList($grid, $filters));

        $grid->onData(function (User $user) {
            $user->fullName = $user->getFullName();

            return $user;
        });

        return view('views.administration.users.index', ['grid' => $grid]);
    }

    public function postIndex()
    {
        if (request()->has('reset'))
        {
            session()->put(self::class.'@index', []);
        }
        else
        {
            session()->put(self::class.'@index', array_filter(request()->get('filters', [])));
        }

        return redirect()->to($this->baseUrl);
    }

    public function getCreate()
    {
        $model = new User();
        $model->fill(request()->old());

        return view('views.administration.users.form', ['model' => $model]);
    }

    public function postCreate()
    {
        $this->validate(request(), userRules()->getUserRules());

        $model = new User();
        $model->email = request()->get('email');
        $model->first_name = request()->get('first_name');
        $model->last_name = request()->get('last_name');
        $model->password = \Hash::make(request()->get('password'));
        $model->save();

        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }

    public function getEdit($id)
    {
        $model = User::query()->findOrFail($id);

        return view('views.administration.users.form', ['model' => $model, 'rules' => userRules()->getUserRules()]);
    }

    public function postEdit($id)
    {
        $model = User::query()->findOrFail($id);

        $this->validate(request(), userRules()->getUserRules(false, $id));

        $model->email = request()->get('email');
        $model->first_name = request()->get('first_name');
        $model->last_name = request()->get('last_name');

        if (!empty(request()->get('password')))
            $model->password = \Hash::make(request()->get('password'));

        $model->save();

        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }

    public function postDelete($id)
    {
        $model = User::query()->findOrFail($id);
        $model->anonymize();
        $model->save();

        return redirect()->to($this->baseUrl);
    }
}
