<?php

namespace App\Admin\Controllers\Cms;

use App\Extensions\Grid\Grid;
use App\Models\Cms\ContactMessage;
use App\Models\Core\User;
use App\Admin\Controllers\DefaultController;
use App\Models\Store\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\View\View;

class ContactController extends DefaultController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->baseUrl .= '/cms/contact';

        view()->composer('views.cms.contact.*', function (View $view){
            $view->with('baseUrl', $this->baseUrl);
        });
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        $grid = new Grid(ContactController::class, 'index');

        $grid->setTitle(__('Wiadomości kontaktowe'));

        $grid->setPagination();
        $filters = session()->get(self::class . '@index', []);
        $grid->setFilters(view('views.cms.contact.filters', ['model' => ['filters' => $filters]])->render());

        $grid->addColumn('id', __('ID'), null, true);
        $grid->addColumn('name', __('Nazwa'), null, true);
        $grid->addColumn('email', __('E-mail'), null, true);
        $grid->addColumn('phone', __('Telefon'), null, true);
        $grid->addColumn('created_at', __('Data przesłania'), null, true);

        $grid->addAction('show', 'id', $this->baseUrl . '/show/{id}');
        $grid->addIcon('show', 'fas fa-question-circle', 'show', '', 'btn btn-default', ['data-information' => '$$message$$']);

        $grid->setData(ContactMessage::getList($grid, $filters));

        return view('views.cms.contact.index', ['grid' => $grid]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex()
    {
        if (request()->has('reset'))
        {
            session()->put(self::class.'@index', []);
        }
        else
        {
            session()->put(self::class.'@index', array_filter(request()->get('filters', [])));
        }

        return redirect()->to($this->baseUrl);
    }

    public function postShow($id)
    {
        $entity = ContactMessage::findOrFail($id);

        return $entity->message;
    }
}
