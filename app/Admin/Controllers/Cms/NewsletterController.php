<?php

namespace App\Admin\Controllers\Cms;

use App\Extensions\Grid\Grid;
use App\Models\Cms\ContactMessage;
use App\Models\Cms\Subscriber;
use App\Models\Core\User;
use App\Admin\Controllers\DefaultController;
use App\Models\Store\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\View\View;

class NewsletterController extends DefaultController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->baseUrl .= '/cms/newsletter';

        view()->composer('views.cms.newsletter.*', function (View $view){
            $view->with('baseUrl', $this->baseUrl);
        });
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        $grid = new Grid(NewsletterController::class, 'index');

        $grid->setTitle(__('Subskrybenci'));

        $grid->setPagination();
        $grid->setSort("is_action_needed");
        $grid->setDirection("desc");
        $filters = session()->get(self::class . '@index', []);
        $grid->setFilters(view('views.cms.newsletter.filters', ['model' => ['filters' => $filters]])->render());

        $grid->addColumn('id', __('ID'), null, true);
        $grid->addColumn('email', __('E-mail'), null, true);
        $grid->addColumn('is_signed', __('Zapisany'));
        $grid->addColumn('is_action_needed', __('Wymagana akcja'));
        $grid->addColumn('created_at', __('Data zapisania'), null, true);

        $grid->addAction('change', 'id', $this->baseUrl . '/change/{id}');
        $grid->addIcon('change', 'fas fa-check-circle', 'change', '', 'btn-default', ['data-confirm' => __('Czy na pewno odznaczyć wiersz?')], 'is_action_needed');

        $grid->setData(Subscriber::getList($grid, $filters));

        $grid->onData(function (Subscriber $row) {
            $row->is_action_needed = $row->is_action_needed ? ($row->is_signed ? __('Przepisz do systemu') : __('Wypisz z systemu')) : '';
            $row->is_signed = $row->is_signed ? __('Tak') : __('Nie');

            return $row;
        });

        return view('views.cms.contact.index', ['grid' => $grid]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex()
    {
        if (request()->has('reset'))
        {
            session()->put(self::class.'@index', []);
        }
        else
        {
            session()->put(self::class.'@index', array_filter(request()->get('filters', [])));
        }

        return redirect()->to($this->baseUrl);
    }

    public function postChange($id)
    {
        $entity = Subscriber::findOrFail($id);

        $entity->is_action_needed = false;
        $entity->save();

        return redirect()->to($this->baseUrl);
    }
}
