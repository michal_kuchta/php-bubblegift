<?php

namespace App\Admin\Controllers;

class DashboardController extends DefaultController
{
    public function getIndex()
    {
        return view('views.dashboard.index');
    }
}
