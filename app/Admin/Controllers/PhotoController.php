<?php

namespace App\Admin\Controllers;

use App\Models\Core\Photo;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Intervention\Image\Facades\Image;

class PhotoController extends DefaultController
{
    public function postStore()
    {
        if(request()->hasFile('photo'))
        {
            $dir = storage_path('/app/public/photos');
            $tempDir = storage_path('/app/public/temp');
            if (!file_exists($dir))
            {
                mkdir($dir,0777,true);
            }

            if (!file_exists($tempDir))
            {
                mkdir($tempDir,0777,true);
            }

            $image = request()->file('photo');
            $imageName = $image->hashName();
            $image->move($tempDir, $imageName);

            $img = Image::make($tempDir.'/'.$imageName);
            $img->resize(500, null, function($constraint){
                $constraint->aspectRatio();
            })->save($dir . '/' . $imageName);

            \Illuminate\Support\Facades\File::delete($tempDir.'/'.$imageName);

            $path = '/storage/photos/' . $imageName;

            $photo = new Photo();
            $photo->created_at = Carbon::now();
            $photo->updated_at = Carbon::now();
            $photo->url = $path;
            $photo->save();

            return response()->json($photo->toArray());
        }

        return response()->json([
            "error" => __('Brak pliku')
        ], 406);
    }
}
