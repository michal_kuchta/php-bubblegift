<?php

namespace App\Admin\Controllers\Store;

use App\Extensions\Grid\Grid;
use App\Models\Core\User;
use App\Admin\Controllers\DefaultController;
use App\Models\Store\Carrier;
use App\Models\Store\Payment;
use App\Models\Store\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\View\View;

class CarriersController extends DefaultController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->baseUrl .= '/store/carriers';

        view()->composer('views.store.carriers.*', function (View $view){
            $view->with('baseUrl', $this->baseUrl);
        });
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        $grid = new Grid(self::class, 'index');

        $grid->setTitle(__('Transporty'));

        $grid->setPagination();
        $grid->setCheckboxes();
        $filters = session()->get(self::class . '@index', []);
        $grid->setFilters(view('views.store.carriers.filters', ['model' => ['filters' => $filters]])->render());

        $grid->addColumn('id', __('ID'), null, true);
        $grid->addColumn('name', __('Nazwa'), null, true);
        $grid->addColumn('price', __('Koszt'), null, false);
        $grid->addColumn('updated_at', __('Data modyfikacji'), null, true);

        $grid->addAction('edit', 'id', $this->baseUrl . '/edit/{id}');
        $grid->addIcon('edit', 'fas fa-edit', 'edit');

        $grid->setData(Carrier::getList($grid, $filters));

        return view('views.store.carriers.index', ['grid' => $grid]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex()
    {
        if (request()->has('reset'))
        {
            session()->put(self::class.'@index', []);
        }
        else
        {
            session()->put(self::class.'@index', array_filter(request()->get('filters', [])));
        }

        return redirect()->to($this->baseUrl);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getCreate()
    {
        $model = new Carrier();
        $model->fill(request()->old());

        return view('views.store.carriers.form', ['model' => $model]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postCreate()
    {
        $this->validate(request(), storeRules()->getCarrierRules());

        $model = new Carrier();
        $model->name = request()->get('name');
        $model->is_active = request()->get('is_active', false);
        $model->price_net = request()->get('price_net', 0);
        $model->save();

        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getEdit($id)
    {
        $model = Carrier::query()->findOrFail($id);

        return view('views.store.carriers.form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postEdit($id)
    {
        $model = Carrier::query()->findOrFail($id);

        $this->validate(request(), storeRules()->getCarrierRules());

        $model->name = request()->get('name');
        $model->is_active = request()->get('is_active', true);
        $model->price_net = request()->get('price_net', 0);

        $model->save();

        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }
}
