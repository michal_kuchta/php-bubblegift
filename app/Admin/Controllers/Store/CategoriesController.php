<?php

namespace App\Admin\Controllers\Store;

use App\Extensions\Grid\Grid;
use App\Models\Core\User;
use App\Admin\Controllers\DefaultController;
use App\Models\Store\Category;
use App\Models\Store\Product;
use App\Models\Store\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\View\View;

class CategoriesController extends DefaultController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->baseUrl .= '/store/categories';

        view()->composer('views.store.categories.*', function (View $view){
            $view->with('baseUrl', $this->baseUrl);
            $view->with('categories', Category::getOptions()->toArray());
        });
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        $grid = new Grid(CategoriesController::class, 'index');

        $grid->setTitle(__('Kategorie'));

        $grid->setPagination();
        $grid->setCheckboxes();
        $filters = session()->get(self::class . '@index', []);
        $grid->setFilters(view('views.store.categories.filters', ['model' => ['filters' => $filters]])->render());

        $grid->addColumn('id', __('ID'), null, true);
        $grid->addColumn('name', __('Nazwa'), null, true);
        $grid->addColumn('slug', __('Symbol'), null, true);
        $grid->addColumn('parent_name', __('Rodzic'), null, true);
        $grid->addColumn('updated_at', __('Data modyfikacji'), null, true);

        $grid->addAction('edit', 'id', $this->baseUrl . '/edit/{id}');
        $grid->addAction('delete', 'deleteColumn', $this->baseUrl . '/delete/{id}');
        $grid->addIcon('edit', 'fas fa-edit', 'edit');
        $grid->addIcon('delete', 'fas fa-times', 'delete', '', 'btn-danger', ['data-confirm' => __('Jesteś pewien usunięcia kategorii? Zostaną również usunięte podkategorie tej kategorii')]);

        $grid->setData(Category::getList($grid, $filters));

        $grid->onData(function (Category $row){
            $row->parent_name = $row->parent ? $row->parent->name : '---';

            return $row;
        });

        return view('views.store.categories.index', ['grid' => $grid]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex()
    {
        if (request()->has('reset'))
        {
            session()->put(self::class.'@index', []);
        }
        else
        {
            session()->put(self::class.'@index', array_filter(request()->get('filters', [])));
        }

        return redirect()->to($this->baseUrl);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getCreate()
    {
        $model = new Category();
        $model->fill(request()->old());

        return view('views.store.categories.form', ['model' => $model]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postCreate()
    {
        $this->validate(request(), storeRules()->getCategoryRules());

        $parent = Category::query()->where('id', intval(request()->get('parent_id')))->first();

        $model = new Category();
        $model->name = request()->get('name');
        $model->slug = \Str::slug($model->name);
        $model->parent_id = request()->get('parent_id');
        $model->level = $parent ? $parent->level + 1 : 1;
        $model->position = 0;
        $model->is_active = request()->get('is_active', true);
        $model->has_children = false;

        $model->save();

        $model->ancestors = ($model->parent_id > 0 && $parent ? $parent->ancestors : '#') . $model->id . '#';
        $model->save();


        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getEdit($id)
    {
        $model = Category::query()->findOrFail($id);

        return view('views.store.categories.form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postEdit($id)
    {
        $model = Category::query()->findOrFail($id);

        $this->validate(request(), storeRules()->getCategoryRules(false, $id));

        $parentId = request()->get('parent_id');
        if (intval($parentId) == $id)
            $parentId = null;

        $parent = Category::query()->where('id', intval($parentId))->first();

        $model->name = request()->get('name');
        $model->slug = \Str::slug($model->name);
        $model->parent_id = $parentId;
        $model->level = $parent ? $parent->level + 1 : 1;
        $model->position = 0;
        $model->is_active = request()->get('is_active', true);
        $model->has_children = false;
        $model->ancestors = '';
        $model->save();

        $model->ancestors = ($model->parent_id > 0 && $parent ? $parent->ancestors : '#') . $model->id . '#';
        $model->save();

        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($id)
    {
        $model = Category::query()->findOrFail($id);

        if ($model)
        {
            $model->delete();
            ProductCategory::query()
                ->whereIn('category_id', Category::query()
                    ->where('ancestors', 'LIKE', "%#$id#%")
                    ->select('id')
                )
                ->delete();
            Category::query()->where('ancestors', 'LIKE', "%#$id#%")->delete();
        }

        return redirect()->to($this->baseUrl);
    }

    public function postSearch()
    {
        $query = trim(request()->input('query'));
        $selected = array_map('intval', request()->input('selected', []));

        if (empty($query))
        {
            return response()->json([
                'categories' => []
            ]);
        }

        $results = Category::query()
            ->where('name', 'LIKE', '%'.$query.'%');

        if (!empty($selected))
        {
            $results->whereNotIn('id', $selected);
        }

        return response()->json([
            'categories' => $results->limit(10)->select(['id', 'name'])->get()->toArray()
        ]);
    }
}
