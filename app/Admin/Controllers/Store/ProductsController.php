<?php

namespace App\Admin\Controllers\Store;

use App\Extensions\Grid\Grid;
use App\Models\Core\User;
use App\Admin\Controllers\DefaultController;
use App\Models\Store\Product;
use App\Models\Store\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\View\View;

class ProductsController extends DefaultController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->baseUrl .= '/store/products';

        view()->composer('views.store.products.*', function (View $view){
            $view->with('baseUrl', $this->baseUrl);
        });
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        $grid = new Grid(ProductsController::class, 'index');

        $grid->setTitle(__('Produkty'));

        $grid->setPagination();
        $grid->setCheckboxes();
        $filters = session()->get(self::class . '@index', []);
        $grid->setFilters(view('views.store.products.filters', ['model' => ['filters' => $filters]])->render());

        $grid->addColumn('id', __('ID'), null, true);
        $grid->addColumn('name', __('Nazwa'), null, true);
        $grid->addColumn('index', __('Symbol'), null, true);
        $grid->addColumn('type', __('Typ'), null, true);
        $grid->addColumn('price', __('Cena'), null, false);
        $grid->addColumn('discount', __('Promocja'), null, false);
        $grid->addColumn('updated_at', __('Data modyfikacji'), null, true);

        $grid->addAction('edit', 'id', $this->baseUrl . '/edit/{id}');
        $grid->addAction('delete', 'deleteColumn', $this->baseUrl . '/delete/{id}');
        $grid->addIcon('edit', 'fas fa-edit', 'edit');
        $grid->addIcon('delete', 'fas fa-times', 'delete', '', 'btn-danger', ['data-confirm' => __('Jesteś pewien usunięcia produktu?')]);

        $grid->setData(Product::getList($grid, $filters));

        $grid->onData(function (Product $model) {
            $model->price = formatPrice($model->getPrice());
            $model->discount = $model->hasDiscount() ? __('Tak') : __('Nie');

            $model->type = $model->type == Product::TYPE_PRODUCT ? __('Produkt') : __('Dodatek');

            return $model;
        });

        return view('views.store.products.index', ['grid' => $grid]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex()
    {
        if (request()->has('reset'))
        {
            session()->put(self::class.'@index', []);
        }
        else
        {
            session()->put(self::class.'@index', array_filter(request()->get('filters', [])));
        }

        return redirect()->to($this->baseUrl);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getCreate()
    {
        $model = new Product();
        $model->fill(request()->old());

        return view('views.store.products.form', ['model' => $model]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postCreate()
    {
        $this->validate(request(), storeRules()->getProductRules());

        $model = new Product();
        $model->name = request()->get('name');
        $model->index = request()->get('index');
        $model->is_active = boolval(request()->get('is_active', false));
        $model->price_net = request()->get('price_net', 0);
        $model->discount_price = request()->get('discount_price', 0);
        $model->discount_percentage = request()->get('discount_percentage', 0);
        $model->tax_rate = request()->get('tax_rate', 23);
        $model->description = request()->get('description');
        $model->category_id = request()->get('category_id');
        $model->images_list = request()->get('images_list', []);
        $model->type = request()->get('type', 'product');
        $model->field_type = request()->get('field_type');
        $model->field_title = request()->get('field_title');
        $model->tags = '#'.implode('#', array_filter(request()->get('tags', []))).'#';
        $model->save();

        $categories = request()->get('categories', []);
        if (!empty($categories))
        {
            foreach ($categories as $category)
            {
                $entity = new ProductCategory();
                $entity->product_id = $model->id;
                $entity->category_id = $category;
                $entity->save();
            }
        }

        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getEdit($id)
    {
        $model = Product::query()->findOrFail($id);

        return view('views.store.products.form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postEdit($id)
    {
        $model = Product::query()->findOrFail($id);

        $this->validate(request(), storeRules()->getProductRules(false, $id));

        $model->name = request()->get('name');
        $model->index = request()->get('index');
        $model->is_active = boolval(request()->get('is_active', false));
        $model->price_net = request()->get('price_net', 0);
        $model->discount_price = request()->get('discount_price', 0);
        $model->discount_percentage = request()->get('discount_percentage', 0);
        $model->tax_rate = request()->get('tax_rate', 23);
        $model->description = request()->get('description');
        $model->category_id = request()->get('category_id');
        $model->images_list = request()->get('images_list', []);
        $model->type = request()->get('type', 'product');
        $model->field_type = request()->get('field_type');
        $model->field_title = request()->get('field_title');
        $model->tags = '#'.implode('#', array_filter(request()->get('tags', []))).'#';

        $model->save();

        $categories = request()->get('categories', []);
        if (!empty($categories))
        {
            ProductCategory::query()->where('product_id', $model->id)->delete();
            foreach ($categories as $category)
            {
                $entity = new ProductCategory();
                $entity->product_id = $model->id;
                $entity->category_id = $category;
                $entity->save();
            }
        }

        return redirect()->to($this->baseUrl . '/edit/' . $model->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($id)
    {
        $model = Product::query()->findOrFail($id);
        $model->delete();
        ProductCategory::query()->where('product_id', $model->id)->delete();

        return redirect()->to($this->baseUrl);
    }
}
