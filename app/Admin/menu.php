<?php

$prefix = config('custom.admin.prefix', 'admin');

return [
    ['href' => "/$prefix", 'name' => __('Dashboard'), 'icon' => 'fas fa-tachometer-alt'],

    ['href' => "/$prefix/administration", 'name' => __('Administracja'), 'icon' => 'fas fa-user-cog', 'clickable' => false, "child" => [
        ['href' => "/$prefix/administration/users", 'name' => __('Użytkownicy'), 'icon' => 'fas fa-users'],
    ]],

    ['href' => "/$prefix/cms", 'name' => __('Moduły'), 'icon' => 'fas fa-boxes', 'clickable' => false, "child" => [
        ['href' => "/$prefix/cms/contact", 'name' => __('Wiadomości kontaktowe'), 'icon' => 'fas fa-envelope'],
        ['href' => "/$prefix/cms/newsletter", 'name' => __('Subskrybenci'), 'icon' => 'fas fa-mail-bulk'],
    ]],

    ['href' => "/$prefix/store", 'name' => __('Sklep'), 'icon' => 'fas fa-store', 'clickable' => false, "child" => [
        ['href' => "/$prefix/store/categories", 'name' => __('Kategorie'), 'icon' => 'fas fa-sitemap'],
        ['href' => "/$prefix/store/products", 'name' => __('Produkty'), 'icon' => 'fas fa-boxes'],
        ['href' => "/$prefix/store/payments", 'name' => __('Płatności'), 'icon' => 'fas fa-cash-register'],
        ['href' => "/$prefix/store/carriers", 'name' => __('Transporty'), 'icon' => 'fas fa-truck'],
    ]],
];
