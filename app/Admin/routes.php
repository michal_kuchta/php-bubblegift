<?php

use App\Admin\Controllers\Cms\ContactController;
use App\Admin\Controllers\Cms\NewsletterController;
use App\Admin\Controllers\Store\CategoriesController;
use Illuminate\Support\Facades\Route;
use \App\Admin\Controllers\DashboardController;
use \App\Admin\Controllers\PhotoController;
use \App\Admin\Controllers\Administration\UsersController;
use \App\Admin\Controllers\Store\ProductsController;
use \App\Admin\Controllers\Store\PaymentsController;
use \App\Admin\Controllers\Store\CarriersController;

Route::get('/', [DashboardController::class, 'getIndex']);
Route::post('/photos/store', [PhotoController::class, 'postStore']);

Route::prefix('administration')->group(function(){
    Route::prefix('users')->group(function(){
        Route::get('/', [UsersController::class, 'getIndex']);
        Route::post('/', [UsersController::class, 'postIndex']);
        Route::get('/create', [UsersController::class, 'getCreate']);
        Route::post('/create', [UsersController::class, 'postCreate']);
        Route::get('/edit/{id}', [UsersController::class, 'getEdit']);
        Route::post('/edit/{id}', [UsersController::class, 'postEdit']);
        Route::post('/delete/{id}', [UsersController::class, 'postDelete']);
    });
});

Route::prefix('store')->group(function(){
    Route::prefix('categories')->group(function(){
        Route::get('/', [CategoriesController::class, 'getIndex']);
        Route::post('/', [CategoriesController::class, 'postIndex']);
        Route::get('/create', [CategoriesController::class, 'getCreate']);
        Route::post('/create', [CategoriesController::class, 'postCreate']);
        Route::get('/edit/{id}', [CategoriesController::class, 'getEdit']);
        Route::post('/edit/{id}', [CategoriesController::class, 'postEdit']);
        Route::post('/delete/{id}', [CategoriesController::class, 'postDelete']);
        Route::post('/search', [CategoriesController::class, 'postSearch']);
    });
    Route::prefix('products')->group(function(){
        Route::get('/', [ProductsController::class, 'getIndex']);
        Route::post('/', [ProductsController::class, 'postIndex']);
        Route::get('/create', [ProductsController::class, 'getCreate']);
        Route::post('/create', [ProductsController::class, 'postCreate']);
        Route::get('/edit/{id}', [ProductsController::class, 'getEdit']);
        Route::post('/edit/{id}', [ProductsController::class, 'postEdit']);
        Route::post('/delete/{id}', [ProductsController::class, 'postDelete']);
    });
    Route::prefix('payments')->group(function(){
        Route::get('/', [PaymentsController::class, 'getIndex']);
        Route::post('/', [PaymentsController::class, 'postIndex']);
        Route::get('/create', [PaymentsController::class, 'getCreate']);
        Route::post('/create', [PaymentsController::class, 'postCreate']);
        Route::get('/edit/{id}', [PaymentsController::class, 'getEdit']);
        Route::post('/edit/{id}', [PaymentsController::class, 'postEdit']);
    });
    Route::prefix('carriers')->group(function(){
        Route::get('/', [CarriersController::class, 'getIndex']);
        Route::post('/', [CarriersController::class, 'postIndex']);
        Route::get('/create', [CarriersController::class, 'getCreate']);
        Route::post('/create', [CarriersController::class, 'postCreate']);
        Route::get('/edit/{id}', [CarriersController::class, 'getEdit']);
        Route::post('/edit/{id}', [CarriersController::class, 'postEdit']);
    });
});

Route::prefix('cms')->group(function(){
    Route::prefix('contact')->group(function(){
        Route::get('/', [ContactController::class, 'getIndex']);
        Route::post('/', [ContactController::class, 'postIndex']);
        Route::post('/show/{id}', [ContactController::class, 'postShow']);
    });
    Route::prefix('newsletter')->group(function(){
        Route::get('/', [NewsletterController::class, 'getIndex']);
        Route::post('/', [NewsletterController::class, 'postIndex']);
        Route::post('/change/{id}', [NewsletterController::class, 'postChange']);
    });
});
