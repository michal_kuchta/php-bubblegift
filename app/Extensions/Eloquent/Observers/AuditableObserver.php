<?php

namespace App\Extensions\Eloquent\Observers;

class AuditableObserver
{
    /**
     * Handle the "creating" event.
     *
     * @param IAuditable $model
     * @return void
     */
    public function creating(IAuditable $model)
    {
        $model->created_at = now();
        $model->updated_at = now();
    }

    /**
     * Handle the "saving" event.
     *
     * @param IAuditable $model
     * @return void
     */
    public function saving(IAuditable $model)
    {
        $model->updated_at = now();
    }

}
