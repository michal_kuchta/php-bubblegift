<?php

namespace App\Extensions\Eloquent\Observers;

/**
 * Class IAuditable
 *
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 */
interface IAuditable
{

}
