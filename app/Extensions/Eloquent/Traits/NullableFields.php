<?php namespace App\Extensions\Eloquent\Traits;

/**
 * Nullable (database) fields trait.
 *
 * Include this trait in any Eloquent models you wish to automatically set
 * empty field values to null on. When saving, iterate over the model's
 * attributes and if their value is empty, make it null before save.
 *
 * @package    Iatstuti
 * @subpackage Database\Support
 * @copyright  2015 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 * @mixin      \Illuminate\Database\Eloquent\Model
 */
trait NullableFields
{
    /**
     * Boot the trait, add a saving observer.
     *
     * When saving the model, we iterate over its attributes and for any attribute
     * marked as nullable whose value is empty, we then set its value to null.
     */
    protected static function bootNullableFields()
    {
        static::saving(function ($model)
        {
            /** @var NullableFields $model */
            foreach ($model->nullableFromArray($model->getAttributes()) as $column => $value)
            {
                if ($model->nullIfEmpty($value))
                {
                    $model->setAttribute($column, null);
                }
            }
        });
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        if ($this->isNullable($key) && $this->nullIfEmpty($value))
        {
            parent::setAttribute($key, null);
        }
        else
        {
            parent::setAttribute($key, $value);
        }

        return $this;
    }

    /**
     * If value is empty, return true, otherwise return false.
     *
     * @param  string $value
     *
     * @return bool
     */
    protected function nullIfEmpty($value)
    {
        return (is_string($value) && trim($value) === '') ? true : false;
    }

    /**
     * Get the nullable attributes of a given array.
     *
     * @param  array $attributes
     *
     * @return array
     */
    protected function nullableFromArray(array $attributes = [ ])
    {
        if (is_array($this->nullable) && count($this->nullable) > 0)
        {
            return array_intersect_key($attributes, array_flip($this->nullable));
        }

        // Assume no fields are nullable
        return [ ];
    }

    /**
     * Checks if column is nullable
     *
     * @param  string $value
     *
     * @return null
     */
    public function isNullable($value)
    {
        return in_array($value, $this->nullable);
    }
}
