<?php namespace App\Extensions\Eloquent\Traits;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Query\Builder;

/**
 * Class UtcDates
 *
 * @package Edito\Extensions\Eloquent\Traits
 *
 * @method whereDatesBetween($column, array $values, $boolean = 'and', $not = false)
 * @method orWhereDatesBetween($column, array $values, $not = false)
 */
trait UtcDates
{
    /**
     * Boot the utc dates trait for a model.
     *
     * @return void
     */
    public static function bootUtcDates()
    {
        Builder::macro('whereDatesBetween', function ($column, array $values, $boolean = 'and', $not = false)
        {
            $values = collect($values)->map(function($value)
            {
                if (config('app.timezone_international') == true)
                {
                    $value = Carbon::parse($value, config('app.timezone_display'));

                    if ($value->getTimezone()->getName() != 'UTC')
                    {
                        $value->setTimezone(new DateTimeZone('UTC'));
                    }
                }
                else
                {
                    $value = Carbon::parse($value);
                }

                return $value;
            });

            $values->last()->addDay()->addSecond(-1);

            return $this->whereBetween($column, $values->toArray(), $boolean, $not);
        });

        Builder::macro('orWhereDatesBetween', function ($column, array $values, $not = false)
        {
            return $this->whereDatesBetween($column, $values, 'or', $not);
        });
    }

    /**
     * Return a timestamp as DateTime object.
     *
     * @param  mixed  $value
     * @return \Carbon\Carbon
     */
    public function asDateTime($value)
    {
        /** @var DateTime $dateTime */
        $dateTime = parent::asDateTime($value);

        if (config('app.timezone_international') == true)
        {
            $timezone = config('app.timezone_display');

            if ($dateTime->getTimezone()->getName() !== $timezone)
            {
                $dateTime->setTimezone(new DateTimeZone($timezone));
            }
        }

        return $dateTime;
    }

    /**
     * Convert a DateTime to a storable string.
     *
     * @param  \DateTime|int  $value
     * @return string
     */
    public function fromDateTime($value)
    {
        if (is_null($value)) return $value;

        /** @var Carbon $value */
        $value = parent::asDateTime($value);

        if (config('app.timezone_international') == true)
        {
            $value = Carbon::parse($value->toDateTimeString(), config('app.timezone_display'));

            if ($value instanceof DateTime && $value->getTimezone()->getName() != 'UTC')
            {
                $value->setTimezone(new DateTimeZone('UTC'));
            }
        }

        return $value->format($this->getDateFormat());
    }
}
