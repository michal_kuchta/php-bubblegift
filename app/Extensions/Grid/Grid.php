<?php

namespace App\Extensions\Grid;

use Illuminate\Support\Collection;

class Grid
{
    private $class = '';
    private $method = '';
    private $title = '';
    private $pageId = 1;
    private $perPage = 20;
    private $maxLinks = 7;
    private $sort = 'id';
    private $direction = 'asc';
    private $pageUrl = '';
    private $totalRows = 0;
    private $filters = '';

    private $data = [];
    private $columns = [];
    private $actions = [];
    private $icons = [];

    private $checkboxes = false;
    private $pagination = false;

    public function __construct($class, $method)
    {
        $this->class = $class;
        $this->method = $method;
        $this->pageUrl = '?pageId=%d';

        $this->setRequestData();
        $this->setSessionData();
    }

    private function setRequestData()
    {
        if (request()->has('pageId'))
            $this->setPageId(intval(request()->get('pageId', $this->pageId)));

        if (request()->has('perPage'))
            $this->setPerPage(intval(request()->get('perPage', $this->perPage)));

        if (request()->has('sort'))
            $this->setSort(request()->get('sort', $this->sort));

        if (request()->has('direction'))
            $this->setDirection(request()->get('direction', $this->direction));
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
        $this->setSessionData();
    }

    private function setSessionData()
    {
        $this->pageId = session("{$this->getClass()}@{$this->getMethod()}.pageId", $this->pageId);
        $this->perPage = session("{$this->getClass()}@{$this->getMethod()}.perPage", $this->perPage);
        $this->sort = session("{$this->getClass()}@{$this->getMethod()}.sort", $this->sort);
        $this->direction = session("{$this->getClass()}@{$this->getMethod()}.direction", $this->direction);
    }
    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
        $this->setSessionData();
    }

    /**
     * @return int
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param int $pageId
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    /**
     * @param array $data
     */
    public function setData($data = [])
    {
        if ($data instanceof Collection)
            $data = $data->all();

        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param \Closure $callback
     */
    public function onData(\Closure $callback)
    {
        $this->data = array_map($callback, $this->data);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param $name
     * @param string $label
     * @param null $size
     * @param bool $sortable
     * @param array $params
     * @param string $display
     */
    public function addColumn($name, $label = '', $size = null, $sortable = false, $params = [], $display = '')
    {
        $this->columns[] = [
            'name' => $name,
            'label' => $label,
            'size' => $size,
            'sortable' => $sortable,
            'params' => $params,
            'display' => $display,
        ];
    }

    /**
     * @return string
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param string $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @return bool
     */
    public function isCheckboxes()
    {
        return $this->checkboxes;
    }

    /**
     * @param bool $checkboxes
     */
    public function setCheckboxes($checkboxes = true)
    {
        $this->checkboxes = $checkboxes;
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortUrl($column)
    {
        $url = request()->getPathInfo();

        $direction = 'asc';

        if ($this->sort == $column)
        {
            if ($this->direction == 'asc')
                $direction = 'desc';
            else
                $direction = 'asc';
        }

        return $url.'?sort='.$column.'&direction='.$direction;
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortIcon($column)
    {
        $icon = 'fas fa-sort-up';

        if ($this->sort == $column)
        {
            if ($this->direction == 'asc')
                $icon = 'fas fa-sort-down';
            else
                $icon = 'fas fa-sort-up';
        }

        return $icon;
    }

    /**
     * @return array
     */
    public function getActions(): array
    {
        return $this->actions;
    }

    /**
     * @param $name
     * @return array
     */
    public function getAction($name): array
    {
        return collect($this->actions)->where('name', '=', $name)->first();
    }

    /**
     * @param $name
     * @param $column
     * @param $url
     */
    public function addAction($name, $column, $url): void
    {
        $this->actions[] = [
            'name' => $name,
            'column' => $column,
            'url' => $url
        ];
    }

    /**
     * @param $name
     * @return boolean
     */
    public function hasAction($name)
    {
        return collect($this->actions)->where('name', '=', $name)->count() > 0;
    }

    /**
     * @param array $actions
     */
    public function setActions(array $actions): void
    {
        $this->actions = $actions;
    }

    /**
     * @return array
     */
    public function getIcons(): array
    {
        return $this->icons;
    }

    /**
     * @param $name
     * @param $icon
     * @param string $action
     * @param string $text
     * @param string $btnClass
     * @param array $attributes
     */
    public function addIcon($name, $icon, $action = '', $text = '', $btnClass = 'btn-default', $attributes = [], $conditionFlag = null): void
    {
        $this->icons[] = [
            'name' => $name,
            'icon' => $icon,
            'action' => $action,
            'text' => $text,
            'btnClass' => $btnClass,
            'attributes' => $attributes,
            'conditionFlag' => $conditionFlag
        ];
    }

    /**
     * @param array $icons
     */
    public function setIcons(array $icons): void
    {
        $this->icons = $icons;
    }

    /**
     * @param $name
     * @param $row
     * @return mixed|string|string[]
     */
    public function parseUrl($name, $row)
    {
        $action = $this->getAction($name);

        if (empty($action))
            return '#';

        $url = $action['url'];

        preg_match_all('/\{(.*?)\}/', $url, $hits);

        foreach ($hits as $hit)
        {
            $hit = str_replace(['{', '}'], '', $hit[0]);
            if(isset($row->{$hit}))
            {
                $url = str_replace("{".$hit."}", $row->{strtolower($hit)}, $url);
            }
        }

        return $url;
    }

    /**
     * @return bool
     */
    public function isPagination(): bool
    {
        return $this->pagination;
    }

    /**
     * @param bool $pagination
     */
    public function setPagination(bool $pagination = true): void
    {
        $this->pagination = $pagination;
    }

    /**
     * @return int[]
     */
    public function getPerPageList()
    {
        return [
            10 => 10,
            20 => 20,
            50 => 50,
            100 => 100,
            200 => 200,
            500 => 500,
        ];
    }

    /**
     * @return int
     */
    public function getRowCount()
    {
        return count($this->getData());
    }

    /**
     * @return int
     */
    public function paginateFrom()
    {
        return $this->getRowCount() == 0 ? 0 : $this->getOffset() + 1;
    }

    /**
     * @return int
     */
    public function paginateTo()
    {
        return min($this->getPageId() * $this->getPerPage(), $this->getRowCount());
    }

    /**
     * BasePager::getOffset()
     * @return int
     */
    public function getOffset()
    {
        return (int)(($this->getPageId() - 1) * $this->getPerPage());
    }

    /**
     * @return int
     */
    public function getTotalPages()
    {
        if ($this->getPerPage() > 0)
            return ceil($this->getTotalRows() / $this->getPerPage());
        else
            return 0;
    }

    /**
     * @return int
     */
    public function getMaxLinks(): int
    {
        return $this->maxLinks;
    }

    /**
     * @param int $maxLinks
     */
    public function setMaxLinks(int $maxLinks): void
    {
        $this->maxLinks = $maxLinks;
    }

    /**
     * @param null $pageId
     * @return string
     */
    public function getPageUrl($pageId = null)
    {
        if (is_null($pageId))
            return $this->pageUrl;

        return sprintf($this->pageUrl, $pageId);
    }

    /**
     * @param string $pageUrl
     */
    public function setPageUrl(string $pageUrl): void
    {
        $this->pageUrl = $pageUrl;
    }

    /**
     * @return int
     */
    public function getTotalRows(): int
    {
        return $this->totalRows;
    }

    /**
     * @param int $totalRows
     */
    public function setTotalRows(int $totalRows): void
    {
        $this->totalRows = $totalRows;
    }

    /**
     * @param $params
     * @return string
     */
    public function renderParams($params, $row = null)
    {
        $return = [];

        foreach ($params as $key => $value)
        {
            if (preg_match('/\$\$(.*)\$\$/', $value))
            {
                $value = str_replace('$', '', $value);
                if (is_null($row) || !isset($row->{$value}))
                {
                    continue;
                }

                $value = htmlspecialchars($row->{$value});
            }

            $return[] = $key . '="' . $value . '"';
        }

        return implode(' ', $return);
    }

    /**
     * @return string
     */
    public function render()
    {
        return view('partials.grid.grid', ['grid' => $this])->render();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}
