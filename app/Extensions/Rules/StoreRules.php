<?php

namespace App\Extensions\Rules;

use App\Models\Core\User;
use App\Models\Store\Product;
use Illuminate\Validation\Rule;

class StoreRules
{
    public function getProductRules($create = true, $id = null)
    {
        $rules = [
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'index' => ['required', 'string', 'min:3', 'max:255'],
            'price_net' => ['required', 'numeric', 'min:0'],
            'discount_price' => ['numeric', 'min:0'],
            'discount_percentage' => ['integer', 'min:0', 'max:100'],
            'tax_rate' => ['required', 'min:1', 'max:100'],
        ];

        if (!$create && !is_null($id))
            $rules['index'][] = Rule::unique(Product::class, 'id')->ignore($id);
        else
            $rules['index'][] = Rule::unique(Product::class);

        return $rules;
    }

    public function getPaymentRules()
    {
        $rules = [
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'price_net' => ['required', 'numeric', 'min:0'],
            'tax_rate' => ['required', 'min:1', 'max:100'],
        ];

        return $rules;
    }

    public function getCarrierRules()
    {
        $rules = [
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'price_net' => ['required', 'numeric', 'min:0'],
            'tax_rate' => ['required', 'min:1', 'max:100'],
        ];

        return $rules;
    }

    public function getCategoryRules($create = true, $id = null)
    {
        $rules = [
            'name' => ['required', 'string', 'min:3', 'max:255'],
        ];

        return $rules;
    }
}
