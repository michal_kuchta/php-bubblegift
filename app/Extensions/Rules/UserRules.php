<?php

namespace App\Extensions\Rules;

use App\Models\Core\User;
use Illuminate\Validation\Rule;

class UserRules
{
    public function getUserRules($create = true, $userId = null)
    {
        $rules = [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
            ],
        ];

        if ($create || (!is_null($userId) && !empty(request()->get('password'))))
            $rules['password'] = [
                'required',
                'string',
                'min:8',
                'confirmed'
            ];

        if (!$create && !is_null($userId))
            $rules['email'][] = Rule::unique(User::class, 'id')->ignore($userId);
        else
            $rules['email'][] = Rule::unique(User::class);

        return $rules;
    }
}
