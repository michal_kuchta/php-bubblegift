<?php

/**
 * @return \App\Models\Core\User|null
 */
function user()
{
    if (auth()->check())
    {
        return auth()->user();
    }

    return null;
}

/**
 * @param float $price
 * @param int $decimals
 * @return string
 */
function formatPrice($price, $decimals = 2)
{
    return is_null($decimals) ? $price : number_format(round(max($price, 0), $decimals, PHP_ROUND_HALF_UP), $decimals, '.', '');
}
