<?php

namespace App\Http\Controllers;

use App\Models\Cms\ContactMessage;
use App\Models\Cms\Subscriber;
use App\Models\Store\Category;
use App\Models\Store\Product;
use App\Models\Store\ProductCategory;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class CatalogController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex($params = null)
    {
        $slug = null;
        $id = null;

        $perPage = 24;
        $pageIndex = intval(request()->get('pageIndex', 1));
        $pageIndex = $pageIndex <= 0 ? 1 : $pageIndex;

        if (!empty($params))
        {
            $params = explode(',', $params);

            $slug = array_shift($params);
            $id = intval(array_shift($params));
        }

        $query = Product::query()
            ->where('is_active', true)
            ->where('type', Product::TYPE_PRODUCT);

        $currentCategory = null;
        if ($id > 0)
        {
            $currentCategory = Category::find($id);
            if ($currentCategory)
            {
                $query->whereIn('id', ProductCategory::query()
                    ->select(['product_id'])
                    ->where('category_id', $id)
                );
            }
        }

        $totalItems = $query->count();

        $products = $query->limit($perPage)->offset(($pageIndex - 1) * $perPage)->get();

        $categories = Category::query()->with(['children', 'children.children', 'children.children.children'])->where('level', 1)->get();

        return view('views.store.catalog.index', compact('products', 'pageIndex', 'currentCategory', 'categories', 'totalItems', 'perPage'));
    }
}
