<?php

namespace App\Http\Controllers;

use App\Models\Cms\ContactMessage;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ContactController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function postSave()
    {
        $rules = [
            'name' => 'required',
            'email' => ['required', 'email'],
            'phone' => ['required'],
            'message' => ['required', 'min:10'],
        ];

        $this->validate(request(), $rules);

        $entity = new ContactMessage();
        $entity->name = request()->input('name');
        $entity->email = request()->input('email');
        $entity->phone = request()->input('phone');
        $entity->message = request()->input('message');
        $entity->created_at = now();
        $entity->updated_at = now();
        $entity->save();

        flash()->success(__('Dziękujemy za przesłanie wiadomości. Odpowiemy najszybciej jak to możliwe'));

        return redirect()->back();
    }
}
