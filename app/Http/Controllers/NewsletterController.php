<?php

namespace App\Http\Controllers;

use App\Models\Cms\ContactMessage;
use App\Models\Cms\Subscriber;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class NewsletterController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function postSave()
    {
        $rules = [
            'newsletter_email' => ['required', 'email', 'unique:cms_subscribers,email']
        ];

        $messages = [
            'newsletter_email.required' => __('Adres e-mail jest wymagany'),
            'newsletter_email.email' => __('Adres e-mail musi być poprawnym adresem e-mail'),
            'newsletter_email.unique' => __('Podany adres e-mail jest już zapisany na newsletter'),
        ];

        $this->validate(request(), $rules, $messages);

        $entity = new Subscriber();
        $entity->email = request()->input('newsletter_email');
        $entity->is_action_needed = true;
        $entity->is_signed = true;
        $entity->created_at = now();
        $entity->updated_at = now();
        $entity->save();

        flash()->success(__('Dziękujemy za zapisanie się do Newslettera'));

        return redirect()->back();
    }
}
