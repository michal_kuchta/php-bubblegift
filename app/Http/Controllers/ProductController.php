<?php

namespace App\Http\Controllers;

use App\Models\Cms\ContactMessage;
use App\Models\Cms\Subscriber;
use App\Models\Store\Category;
use App\Models\Store\Product;
use App\Models\Store\ProductCategory;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ProductController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex($params)
    {
        $params = explode(',', $params);

        $slug = array_shift($params);
        $id = intval(array_shift($params));

        /** @var Product $product */
        $product = Product::query()
            ->where('is_active', true)
            ->where('type', Product::TYPE_PRODUCT)
            ->find($id);

        $referer = request()->server('HTTP_REFERER');

        $category = null;
        if (!empty($referer) && \Str::contains($referer, url()->route('store.catalog')))
        {
            preg_match('/\/([^\/]+),(\d+)$/', $referer, $matches);

            if (count($matches) == 3)
            {
                $categoryId = intval(last($matches));

                if ($categoryId > 0)
                {
                    $category = Category::query()->find($categoryId);
                }
            }
        }

        if (!$category)
        {
            $category = $product->categories->first();
        }

        $breadCrumbs = [
            ['url' => url('/'), 'name' => __('Strona główna')],
        ];

        if ($category)
        {
            $breadCrumbs = array_merge($breadCrumbs, $category->getBreadcrumbs());
        }

        $breadCrumbs[] = [
            'url' => route('store.product', [str_slug($product->name) . ',' . $product->id]),
            'name' => $product->name
        ];

        $addons = Product::query()
            ->where('is_active', true)
            ->where('type', Product::TYPE_ADDON)
            ->orderBy('field_type', 'asc')
            ->get();

        if (!$product)
            abort(404);

        return view('views.store.product.index', compact('product', 'addons', 'breadCrumbs'));
    }
}
