<?php namespace App\Models\Cms;

use App\Extensions\Grid\Grid;
use App\Models\Cms\Base\ContactMessageAbstract;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ContactMessage
 *
 * @package App\Models\Cms
 */
class ContactMessage extends ContactMessageAbstract
{
    public static function getList(Grid $grid = null, array $filters = [])
    {
        $query = self::filter(self::query(), $filters);

        $pageId = 1;
        $perPage = 10;
        $sort = 'id';
        $direction = 'asc';

        if ($grid)
        {
            $pageId = $grid->getPageId();
            $perPage = $grid->getPerPage();
            $sort = $grid->getSort();
            $direction = $grid->getDirection();
        }

        $grid->setTotalRows($query->count());

        return $query
            ->offset(($pageId-1) * $perPage)
            ->limit($perPage)
            ->orderBy($sort, $direction)
            ->get();
    }

    private static function filter(Builder $query, $filters = [])
    {
        if (array_has($filters, 'name') && !empty(array_get($filters, 'name')))
        {
            $query->orWhereRaw('name LIKE ?', ["%".array_get($filters, 'name')."%"]);
            $query->orWhereRaw('email LIKE ?', ["%".array_get($filters, 'name')."%"]);
            $query->orWhereRaw('phone LIKE ?', ["%".array_get($filters, 'name')."%"]);
        }

        return $query;
    }
}
