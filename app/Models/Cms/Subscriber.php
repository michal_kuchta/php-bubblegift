<?php namespace App\Models\Cms;

use App\Extensions\Grid\Grid;
use App\Models\Cms\Base\SubscriberAbstract;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Subscriber
 *
 * @package App\Models\Cms
 */
class Subscriber extends SubscriberAbstract
{
    public static function getList(Grid $grid = null, array $filters = [])
    {
        $query = self::filter(self::query(), $filters);

        $pageId = 1;
        $perPage = 10;
        $sort = 'id';
        $direction = 'asc';

        if ($grid)
        {
            $pageId = $grid->getPageId();
            $perPage = $grid->getPerPage();
            $sort = $grid->getSort();
            $direction = $grid->getDirection();
        }

        $grid->setTotalRows($query->count());

        return $query
            ->offset(($pageId-1) * $perPage)
            ->limit($perPage)
            ->orderBy($sort, $direction)
            ->get();
    }

    private static function filter(Builder $query, $filters = [])
    {
        if (array_has($filters, 'email') && !empty(array_get($filters, 'email')))
        {
            $query->orWhereRaw('email LIKE ?', ["%".array_get($filters, 'email')."%"]);
        }

        return $query;
    }
}
