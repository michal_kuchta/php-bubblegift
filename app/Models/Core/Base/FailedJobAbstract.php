<?php namespace App\Models\Core\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;
use App\Extensions\Eloquent\Traits\UtcDates;

/**
 * Class FailedJobAbstract
 *
 * @package App\Models\Core\Base
 *
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 *
 * @property integer id
 * @property string uuid
 * @property string connection
 * @property string queue
 * @property string payload
 * @property string exception
 * @property \Carbon\Carbon failed_at
 */
abstract class FailedJobAbstract extends Model
{
    use FixedFields, NullableFields, UtcDates;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'core_failed_jobs';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var string
     */
    protected $properties = [
        'id',
        'uuid',
        'connection',
        'queue',
        'payload',
        'exception',
        'failed_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'failed_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [

    ];
}
