<?php namespace App\Models\Core\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;
use App\Extensions\Eloquent\Traits\UtcDates;

/**
 * Class PasswordResetAbstract
 *
 * @package App\Models\Core\Base
 *
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 *
 * @property string email
 * @property string token
 * @property \Carbon\Carbon created_at
 */
abstract class PasswordResetAbstract extends Model
{
    use FixedFields, NullableFields, UtcDates;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'core_password_resets';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var string
     */
    protected $properties = [
        'email',
        'token',
        'created_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'created_at'
    ];
}
