<?php namespace App\Models\Core\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;
use App\Extensions\Eloquent\Traits\UtcDates;

/**
 * Class UserAbstract
 *
 * @package App\Models\Core\Base
 *
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 *
 * @property integer id
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property \Carbon\Carbon email_verified_at
 * @property string password
 * @property string two_factor_secret
 * @property string two_factor_recovery_codes
 * @property boolean is_admin
 * @property string remember_token
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property boolean is_active
 * @property boolean is_anonymized
 */
abstract class UserAbstract extends Model
{
    use FixedFields, NullableFields, UtcDates;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'core_users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var string
     */
    protected $properties = [
        'id',
        'first_name',
        'last_name',
        'email',
        'email_verified_at',
        'password',
        'two_factor_secret',
        'two_factor_recovery_codes',
        'is_admin',
        'remember_token',
        'created_at',
        'updated_at',
        'is_active',
        'is_anonymized'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'email_verified_at',
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'email_verified_at',
        'two_factor_secret',
        'two_factor_recovery_codes',
        'remember_token',
        'created_at',
        'updated_at'
    ];
}
