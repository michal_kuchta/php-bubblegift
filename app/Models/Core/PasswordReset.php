<?php namespace App\Models\Core;

use App\Models\Core\Base\PasswordResetAbstract;

/**
 * Class PasswordReset
 *
 * @package App\Models\Core
 */
class PasswordReset extends PasswordResetAbstract
{
}
