<?php namespace App\Models\Core;

use App\Extensions\Eloquent\Observers\AuditableObserver;
use App\Extensions\Eloquent\Observers\IAuditable;
use App\Extensions\Grid\Grid;
use App\Models\Core\Base\UserAbstract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 *
 * @package App\Models\Core
 */
class User extends UserAbstract implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    IAuditable
{
    use HasFactory, Notifiable, Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();
        self::observe(AuditableObserver::class);
    }

    public static function query() : Builder
    {
        return parent::query()->where('is_anonymized', 0);
    }

    public static function getUsersList(Grid $grid = null, array $filters = [])
    {
        $query = self::filter(self::query(), $filters);

        $pageId = 1;
        $perPage = 10;
        $sort = 'id';
        $direction = 'asc';

        if ($grid)
        {
            $pageId = $grid->getPageId();
            $perPage = $grid->getPerPage();
            $sort = $grid->getSort();
            $direction = $grid->getDirection();
        }

        $grid->setTotalRows($query->count());

        return $query
            ->offset(($pageId-1) * $perPage)
            ->limit($perPage)
            ->orderBy($sort, $direction)
            ->get();
    }

    private static function filter(Builder $query, $filters = [])
    {
        if (array_has($filters, 'email') && !empty(array_get($filters, 'email')))
        {
            $query->whereRaw('email LIKE ?', ["%".array_get($filters, 'email')."%"]);
        }

        if (array_has($filters, 'name') && !empty(array_get($filters, 'name')))
        {
            $query->whereRaw('first_name LIKE ?', ["%".array_get($filters, 'name')."%"]);
            $query->whereRaw('last_name LIKE ?', ["%".array_get($filters, 'name')."%"]);
        }

        return $query;
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function anonymize()
    {
        $this->first_name = 'Anonymized First Name ' . $this->id;
        $this->last_name = 'Anonymized Last Name ' . $this->id;
        $this->email = 'user_'.$this->id.'@bubblegiftmail.com';
        $this->is_admin = false;
        $this->is_active = false;
        $this->is_anonymized = true;
        $this->password = \Hash::make(str_random(8));

        $this->save();
    }
}
