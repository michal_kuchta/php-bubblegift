<?php namespace App\Models\Store\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;
use App\Extensions\Eloquent\Traits\UtcDates;

/**
 * Class CarrierAbstract
 *
 * @package App\Models\Store\Base
 *
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 *
 * @property integer id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property string name
 * @property float price_net
 * @property integer tax_rate
 * @property boolean is_active
 */
abstract class CarrierAbstract extends Model
{
    use FixedFields, NullableFields, UtcDates;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store_carriers';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var string
     */
    protected $properties = [
        'id',
        'created_at',
        'updated_at',
        'name',
        'price_net',
        'tax_rate',
        'is_active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'created_at',
        'updated_at'
    ];
}
