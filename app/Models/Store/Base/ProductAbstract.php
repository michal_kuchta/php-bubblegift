<?php namespace App\Models\Store\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;
use App\Extensions\Eloquent\Traits\UtcDates;

/**
 * Class ProductAbstract
 *
 * @package App\Models\Store\Base
 *
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 *
 * @property integer id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property string name
 * @property string index
 * @property boolean is_active
 * @property float price_net
 * @property float discount_price
 * @property float discount_percentage
 * @property integer tax_rate
 * @property string description
 * @property integer category_id
 * @property mixed images_list
 * @property string tags
 * @property string type
 * @property string field_type
 * @property string field_title
 */
abstract class ProductAbstract extends Model
{
    use FixedFields, NullableFields, UtcDates;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store_products';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var string
     */
    protected $properties = [
        'id',
        'created_at',
        'updated_at',
        'name',
        'index',
        'is_active',
        'price_net',
        'discount_price',
        'discount_percentage',
        'tax_rate',
        'description',
        'category_id',
        'images_list',
        'tags',
        'type',
        'field_type',
        'field_title'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'created_at',
        'updated_at',
        'description',
        'category_id',
        'images_list',
        'field_type',
        'field_title'
    ];
}
