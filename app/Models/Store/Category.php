<?php namespace App\Models\Store;

use App\Extensions\Grid\Grid;
use App\Models\Store\Base\CategoryAbstract;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Category
 *
 * @package App\Models\Store
 */
class Category extends CategoryAbstract
{
    protected $fillable = [
        'name',
        'parent_id',
        'slug',
        'ancestors',
        'level',
        'position',
        'is_active',
        'has_children'
    ];

    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public static function getList(Grid $grid = null, array $filters = [])
    {
        $query = self::filter(self::query(), $filters);

        $pageId = 1;
        $perPage = 10;
        $sort = 'id';
        $direction = 'asc';

        if ($grid)
        {
            $pageId = $grid->getPageId();
            $perPage = $grid->getPerPage();
            $sort = $grid->getSort();
            $direction = $grid->getDirection();
        }

        $grid->setTotalRows($query->count());

        return $query
            ->offset(($pageId-1) * $perPage)
            ->limit($perPage)
            ->orderBy($sort, $direction)
            ->get();
    }

    private static function filter(Builder $query, $filters = [])
    {
        if (array_has($filters, 'name') && !empty(array_get($filters, 'name')))
        {
            $query->whereRaw('name LIKE ?', ["%".array_get($filters, 'name')."%"]);
        }

        if (array_has($filters, 'parent_id') && !empty(array_get($filters, 'parent_id')))
        {
            $query->where('parent_id', array_get($filters, 'parent_id'));
        }

        return $query;
    }

    public function getBreadcrumbs()
    {
        $ancestors = self::query()
            ->whereIn('id', explode('#', trim($this->ancestors,'#')))
            ->where('id', '!=', $this->id)
            ->get();

        $breadCrumbs = [];
        foreach (explode('#', trim($this->ancestors,'#')) as $id)
        {
            if ($id == $this->id)
                continue;

            $category = $ancestors->where('id', $id)->first();
            $breadCrumbs[] = [
                'url' => route('store.catalog', [str_slug($category->name) . ',' . $category->id]),
                'name' => $category->name
            ];
        }

        return $breadCrumbs;
    }

    public static function getOptions()
    {
        return self::pluck('name', 'id');
    }
}
