<?php namespace App\Models\Store;

use App\Extensions\Eloquent\Observers\AuditableObserver;
use App\Extensions\Eloquent\Observers\IAuditable;
use App\Extensions\Grid\Grid;
use App\Models\Store\Base\PaymentAbstract;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Payment
 *
 * @package App\Models\Store
 */
class Payment extends PaymentAbstract implements IAuditable
{
    public static function boot()
    {
        parent::boot();

        self::observe(AuditableObserver::class);
    }

    public static function getList(Grid $grid = null, array $filters = [])
    {
        $query = self::filter(self::query(), $filters);

        $pageId = 1;
        $perPage = 10;
        $sort = 'id';
        $direction = 'asc';

        if ($grid)
        {
            $pageId = $grid->getPageId();
            $perPage = $grid->getPerPage();
            $sort = $grid->getSort();
            $direction = $grid->getDirection();
        }

        $grid->setTotalRows($query->count());

        return $query
            ->offset(($pageId-1) * $perPage)
            ->limit($perPage)
            ->orderBy($sort, $direction)
            ->get();
    }

    private static function filter(Builder $query, $filters = [])
    {
        if (array_has($filters, 'name') && !empty(array_get($filters, 'name')))
        {
            $query->whereRaw('name LIKE ?', ["%".array_get($filters, 'name')."%"]);
        }

        return $query;
    }
}
