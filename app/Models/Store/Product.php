<?php namespace App\Models\Store;

use App\Extensions\Eloquent\Observers\AuditableObserver;
use App\Extensions\Eloquent\Observers\IAuditable;
use App\Extensions\Grid\Grid;
use App\Models\Core\Photo;
use App\Models\Store\Base\ProductAbstract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class Product
 *
 * @var Collection $categories
 * @package App\Models\Store
 */
class Product extends ProductAbstract implements IAuditable
{
    const TYPE_PRODUCT = 'product';
    const TYPE_ADDON = 'addon';

    protected $casts = [
        'images_list' => 'array'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public static function boot()
    {
        parent::boot();

        self::observe(AuditableObserver::class);
    }

    public function images()
    {
        if ($this->id > 0 && !empty($this->images_list) && !$this->relationLoaded('imageslist'))
        {
            $images = Photo::query()->whereIn('id', $this->images_list)->get();
            $this->setRelation('imageslist', $images);
        }
        elseif (!$this->relationLoaded('imageslist'))
        {
            $this->setRelation('imageslist', collect());
        }

        return $this->imageslist;
    }

    public function categories()
    {
        return $this->hasManyThrough(Category::class, ProductCategory::class, "product_id", "id", "id", "category_id");
    }

    public static function getList(Grid $grid = null, array $filters = [])
    {
        $query = self::filter(self::query(), $filters);

        $pageId = 1;
        $perPage = 10;
        $sort = 'id';
        $direction = 'asc';

        if ($grid)
        {
            $pageId = $grid->getPageId();
            $perPage = $grid->getPerPage();
            $sort = $grid->getSort();
            $direction = $grid->getDirection();
        }

        $grid->setTotalRows($query->count());

        return $query
            ->offset(($pageId-1) * $perPage)
            ->limit($perPage)
            ->orderBy($sort, $direction)
            ->get();
    }

    private static function filter(Builder $query, $filters = [])
    {
        if (array_has($filters, 'name') && !empty(array_get($filters, 'name')))
        {
            $query->whereRaw('name LIKE ?', ["%".array_get($filters, 'name')."%"]);
        }

        if (array_has($filters, 'index') && !empty(array_get($filters, 'index')))
        {
            $query->where('index', array_get($filters, 'index'));
        }

        if (array_has($filters, 'price_from') && !empty(array_get($filters, 'price_from')))
        {
            $query->where('price', '>=',  array_get($filters, 'price_from'));
        }

        if (array_has($filters, 'price_to') && !empty(array_get($filters, 'price_to')))
        {
            $query->where('price', '<',  array_get($filters, 'price_to'));
        }

        if (array_has($filters, 'type') && !empty(array_get($filters, 'type')))
        {
            $query->where('type',  array_get($filters, 'type'));
        }

        return $query;
    }

    public function hasDiscount()
    {
        return $this->discount_price > 0 || $this->discount_percentage > 0;
    }

    public function getDiscountPriceNet()
    {
        if ($this->discount_price > 0)
            return $this->discount_price;
        else
            return $this->price_net * ((100 - $this->discount_percentage) / 100);
    }

    public function getDiscountPrice()
    {
        return $this->getDiscountPriceNet() * ((100 - $this->tax_rate) / 100);
    }

    public function getPriceNet()
    {
        $price = $this->price_net;

        if ($this->hasDiscount())
            $price = $this->getDiscountPriceNet();

        return $price;
    }

    public function getPrice()
    {
        return $this->getPriceNet() * ((100 + $this->tax_rate) / 100);
    }

    public function getTags()
    {
        $data = array_filter(explode('#', $this->tags));

        if (empty($data))
            $data[] = '';

        return array_values($data);
    }

    public static function getProductsForTag($tag, $limit = 10)
    {
        return self::query()
            ->where('type', self::TYPE_PRODUCT)
            ->whereRaw('tags LIKE ?', ["%#$tag#%"])
            ->where('is_active', 1)
            ->limit($limit)
            ->get();
    }

    public static function getRecommendedProducts($limit = 10)
    {
        return self::query()
            ->where('type', self::TYPE_PRODUCT)
            ->where('is_active', 1)
            ->inRandomOrder()
            ->limit($limit)
            ->get();
    }
}
