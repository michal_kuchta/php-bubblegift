<?php

namespace App\Providers;

use App\Admin\Composers\MenuComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Illuminate\View\FileViewFinder;
use Illuminate\View\ViewServiceProvider as ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $isAdmin = Str::startsWith(ltrim(request()->getPathInfo(), '/'), config('custom.admin.prefix', 'admin'));

        if ($isAdmin)
        {
            View::composer('layouts.partials.menu', MenuComposer::class);
            \File::getRequire(base_path('resources/admin/blade.php'));
        }

    }

    /**
     * Register the view finder implementation.
     *
     * @return void
     */
    public function registerViewFinder()
    {
        $isAdmin = Str::startsWith(ltrim(request()->getPathInfo(), '/'), config('custom.admin.prefix', 'admin'));

        $this->app->bind('view.finder', function ($app) use ($isAdmin) {

            return new FileViewFinder($app['files'], $isAdmin ? $app['config']['view.admin_paths'] : $app['config']['view.paths']);
        });
    }
}
