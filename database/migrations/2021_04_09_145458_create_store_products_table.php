<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('index');
            $table->boolean('is_active')->default(true);
            $table->float('price_net')->default(0);
            $table->float('discount_price')->default(0);
            $table->float('discount_percentage')->default(0);
            $table->integer('tax_rate')->default(23);
            $table->text('description')->nullable();
            $table->bigInteger('category_id')->nullable();
            $table->json('images_list')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_products');
    }
}
