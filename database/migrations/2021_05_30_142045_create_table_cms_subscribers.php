<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCmsSubscribers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_subscribers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('email');
            $table->boolean('is_action_needed')->default(false);
            $table->boolean('is_signed')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_subscribers');
    }
}
