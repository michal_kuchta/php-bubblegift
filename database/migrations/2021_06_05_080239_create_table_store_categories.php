<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStoreCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_categories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('slug');
            $table->bigInteger('parent_id')->nullable();
            $table->string('ancestors')->nullable();

            $table->integer('level')->nullable();
            $table->integer('position')->nullable();
            $table->boolean('is_active')->default(false);
            $table->boolean('has_children')->default(false);

        });
        Schema::create('store_product_categories', function (Blueprint $table) {
            $table->bigInteger('category_id');
            $table->bigInteger('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_categories');
        Schema::dropIfExists('store_product_categories');
    }
}
