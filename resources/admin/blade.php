<?php

if (!function_exists('seo'))
{
    /**
     * @return \App\Extensions\Rules\UserRules
     */
    function userRules()
    {
        return app(\App\Extensions\Rules\UserRules::class);
    }
}

if (!function_exists('seo'))
{
    /**
     * @return \App\Extensions\Rules\StoreRules
     */
    function storeRules()
    {
        return app(\App\Extensions\Rules\StoreRules::class);
    }
}
