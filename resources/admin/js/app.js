require('./bootstrap');

$(document).ready(function (){
    $('#mainMenu').metisMenu();

    $('[data-confirm]')
        .popover({
            html: true,
            container: 'body',
            placement: 'left',
            animation: false,
            template: '<div class="popover" data-toggle="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
            content: function ()
            {
                var $this = $(this);
                var content = $('<div class="confirm"></div>');
                var buttons = $('<div class="btn-group"></div>');
                var eventName = $this.data('confirm-event');

                var btn = $('<a href="#" class="btn btn-default btn-sm"></a>');
                btn.append('<i class="fas fa-check"></i>').click(function ()
                {
                    $.ajax({type: 'POST', url: $this.attr('href'), data: {confirm: true}})
                        .done(function (result)
                        {
                            if (result.redirect)
                            {
                                location = result.redirect;
                                return;
                            }
                            location.reload(true);
                        });

                    $this.popover('hide');
                });
                buttons.append(btn);

                var btn = $('<a href="#" class="btn btn-danger btn-sm"></a>');
                btn.append('<i class="fas fa-times"></i>').click(function ()
                {
                    $this.popover('hide');
                });
                buttons.append(btn);

                buttons.find('.btn:first').addClass('btn-primary');

                content.append('<div class="txt">' + $this.data('confirm') + '</div>');
                content.append(buttons);

                return content;
            }
        })
        .click(function ()
        {
            return false;
        });

    $('[data-information]')
        .popover({
            html: true,
            container: 'body',
            placement: 'left',
            animation: false,
            template: '<div class="popover" data-toggle="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
            content: function ()
            {
                var $this = $(this);
                var content = $('<div class="confirm"></div>').html($this.data('information'));

                return content;
            }
        })
        .click(function ()
        {
            return false;
        });
});

tinyMCE.init({
    height : "480"
});
