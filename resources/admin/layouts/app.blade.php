<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="/admin/css/vendor.css" rel="stylesheet">
    <link href="/admin/css/app.css" rel="stylesheet">

    <script src="/admin/js/vendor.js"></script>
    <script src="https://cdn.tiny.cloud/1/cg3bnhh4mjkf0n20d1x36s980d8ynts6df7fvixhye72x6gf/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>

    <script src="/admin/js/app.js"></script>
</head>
    <body class="antialiased">
        @include('layouts.partials.menu')

        <main>
            @yield('content')
        </main>

        {!! Fluent::scripts(false) !!}
    </body>
</html>
