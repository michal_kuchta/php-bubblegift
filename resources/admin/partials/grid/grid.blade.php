@php
/**
 * @var \App\Extensions\Grid\Grid $grid
 */
@endphp
<div class="card-default card">
    @if(!empty($grid->getTitle()))
        <div class="card-header">{{ $grid->getTitle() }}</div>
    @endif
    <div class="gridview">
        @if(!empty($grid->getFilters()))
            <div class="filters">
                {!! $grid->getFilters() !!}
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered table-hover mb-0">
                <colgroup>
                    @if ($grid->isCheckboxes())
                        <col style="width: 25px" />
                    @endif
                    @foreach ($grid->getColumns() as $column)
                        @if (!empty($column['size']) && $column['size'] > 0)
                            <col style="width: {{ $column['size'] }}px"{!! $grid->renderParams($column['params']) !!} />
                        @else
                            <col{!! $grid->renderParams($column['params']) !!} />
                        @endif
                    @endforeach
                    @if (count($grid->getIcons()) > 0)
                        <col />
                    @endif
                </colgroup>
                <thead>
                <tr>
                    @if ($grid->isCheckboxes())
                        <th>
                            <input type="checkbox" data-grid-action="checkbox">
                        </th>
                    @endif
                    @foreach ($grid->getColumns() as $column)
                        <th{!! $grid->renderParams($column['params']) !!}>
                            @if ($column['sortable'])
                                <a href="{{ $grid->getSortUrl($column['name']) }}">
                                    @if ($grid->getSort() == $column['name'])
                                        <i class="{{ $grid->getSortIcon($column['name']) }}"></i>
                                    @endif
                                    {{ $column['label'] }}
                                </a>
                            @else
                                {{ $column['label'] }}
                            @endif
                        </th>
                    @endforeach
                    @if (count($grid->getIcons()) > 0)
                        <th></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach ($grid->getData() as $row)
                    <tr data-grid-rowid="{{ $row->id }}">
                        @if ($grid->isCheckboxes())
                            <td>
                                <input type="checkbox" data-grid-action="checkbox" value="{{ $row->id }}">
                            </td>
                        @endif
                        @foreach ($grid->getColumns() as $column)
                            <td{!! $grid->renderParams($column['params']) !!}>
                                @if ($grid->hasAction($column['name']))
                                    <a href="{{ $grid->parseUrl($column['name'], $row) }}">
                                        @if (isset($column['display']) && !empty($column['display']))
                                            {!! $row->{$column['display']} !!}
                                        @else
                                            {!! $row->{$column['name']} !!}
                                        @endif
                                    </a>
                                @else
                                    @if (isset($column['display']) && !empty($column['display']))
                                        {!! $row->{$column['display']} !!}
                                    @else
                                        {!! $row->{$column['name']} !!}
                                    @endif
                                @endif
                            </td>
                        @endforeach
                        @if (count($grid->getIcons()) > 0)
                            <td class="buttons text-right">
                                <div class="btn-group">
                                    @foreach ($grid->getIcons() as $icon)
                                        @if (empty($icon['conditionFlag']) || $row->{$icon['conditionFlag']} == true)
                                        <a class="btn {{ $icon['btnClass'] }}" href="{{ $grid->parseUrl($icon['action'], $row) }}" title="{{ $icon['text'] }}" {!! $grid->renderParams($icon['attributes'], $row) !!}>
                                            <i class="{{ $icon['icon'] }}"></i>
                                        </a>
                                        @endif
                                    @endforeach
                                </div>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if (count($grid->getData()) == 0)
                <div class="empty text-center">{{ __('Nie znaleziono żadnych wyników.') }}</div>
            @endif
        </div>
        @if ($grid->isPagination() && count($grid->getData()) > 0)
            <div class="footer p-2">
                <div class="stats">
                    {{ sprintf('Wyświetlam %s - %s z %s', $grid->paginateFrom(), $grid->paginateTo(), $grid->getRowCount()) }}
                </div>
                @if ($grid->getTotalPages() > 1)
                    <ul class="pagination pagination-sm">
                        @if ($grid->getPageId() == 1)
                            <li class="disabled"><span>{{ __('Pierwsza') }}</span></li>
                        @else
                            <li><a href="{{ $grid->getPageUrl(1) }}">{{ __('Pierwsza') }}</a></li>
                        @endif

                        @if ($grid->getPageId() == 1)
                            <li class="disabled hidden-xs"><span>{{ __('Poprzednia') }}</span></li>
                        @else
                            <li class="hidden-xs"><a href="{{ $grid->getPageUrl($grid->getPageId() - 1) }}">{{ __('Poprzednia') }}</a></li>
                        @endif

                        @for ($i = max(1, $grid->getPageId() - $grid->getMaxLinks()); $i <= min($grid->getPageId() + $grid->getMaxLinks(), $grid->getTotalPages()); $i++)
                            @if ($grid->getPageId() == $i)
                                <li class="active"><span>{{ $i }}</span></li>
                            @else
                                <li><a href="{{ $grid->getPageUrl($i) }}">{{ $i }}</a></li>
                            @endif
                        @endfor

                        @if ($grid->getPageId() == $grid->getTotalPages())
                            <li class="disabled hidden-xs"><span>{{ __('Następna') }}</span></li>
                        @else
                            <li class="hidden-xs"><a href="{{ $grid->getPageUrl($grid->getPageId() + 1) }}">{{ __('Następna') }}</a></li>
                        @endif

                        @if ($grid->getPageId() == $grid->getTotalPages())
                            <li class="disabled"><span>{{ __('Ostatnia') }}</span></li>
                        @else
                            <li><a href="{{ $grid->getPageUrl($grid->getTotalPages()) }}">{{ __('Ostatnia') }}</a></li>
                        @endif
                    </ul>
                @endif
            </div>
        @endif
    </div>
</div>
