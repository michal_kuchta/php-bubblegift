@if (!empty($items))
    <ul @if($level == 0) id="mainMenu" @endif>
        @foreach ($items as $item)
            @php
                if (array_get($item, 'href', '#') == '/'.config('custom.admin.prefix'))
                    $isActive = request()->getPathInfo() == '/'.config('custom.admin.prefix');
                else
                    $isActive = \Str::contains(request()->getPathInfo(), array_get($item, 'href', '#'));
            @endphp
            <li class="{{ $isActive ? 'mm-active' : '' }}">
                <a
                    class="{{ isset($item['child']) && !empty($item['child']) ? "has-arrow" : '' }}"
                    href="{{ boolval(array_get($item, 'clickable', true)) ? array_get($item, 'href', '#') : '#' }}"
                    aria-expanded="{{ $isActive ? 'true' : 'false' }}"
                >
                    @if(isset($item['icon']) && !empty($item['icon']))
                        <i class="{{ $item['icon'] }}"></i>
                    @endif
                    <span>{!! array_get($item, 'name', '') !!}</span>
                </a>
                @include('partials.mainMenu', ['items' => array_get($item, 'child', []), 'level' => $level+1])
            </li>
        @endforeach
    </ul>
@endif
