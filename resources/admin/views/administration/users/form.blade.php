@extends('layouts.app')

@section('content')
    <h1>{!! $model->id > 0 ? __('Tworzenie nowego użytkownika') : __('Edycja użytkownika') !!}</h1>

    {!! FluentForm::standard($model)->attr('autocomplete', 'off') !!}

    <div class="row">
        <div class="col-lg-6">
            {!! FluentForm::group()->text('email')->label(__('E-mail')) !!}
            {!! FluentForm::group()->password('password')->label(__('Hasło')) !!}
            {!! FluentForm::group()->password('password_confirmation')->label(__('Powtórz hasło')) !!}
            {!! FluentForm::group()->text('first_name')->label(__('Imię')) !!}
            {!! FluentForm::group()->text('last_name')->label(__('Nazwisko')) !!}
            @if(user()->is_admin)
                {!! FluentForm::checkbox('is_admin')->label(__('Uprawnienia administratora')) !!}
            @endif
        </div>
        @if (!empty($errors->all()))
            <div class="col-lg-3">
                <div class="card border-danger mb-3">
                    <div class="card-header">{!! __('Błędy formularza') !!}</div>
                    <div class="card-body text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>

    {!! FluentForm::footer([
        FluentForm::submit('save', __('Zapisz'))->css(['btn-success']),
        Fluent::link(url($baseUrl), __('Powrót'))->css(['btn-default'])
    ]) !!}
    {!! FluentForm::close() !!}
@endsection
