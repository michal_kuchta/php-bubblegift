@extends('layouts.app')

@section('content')
    {!! $grid !!}

    {!! FluentForm::footer([
        Fluent::link(url($baseUrl.'/create'), __('Dodaj użytkownika'))->css(['btn-primary'])
    ]) !!}
@endsection
