{!! FluentForm::standard($model) !!}
<div class="p-2">
    <div class="row">
        <div class="col-lg-11">
            <div class="row">
                {!! FluentForm::group()->css('col-lg-6')->text('filters[email]')->placeholder(__('Email')) !!}
            </div>
        </div>
        <div class="col-lg-1 text-right">
            {!! FluentForm::submit('save', '')->css('btn btn-default')->title(__('Filtruj'))->icon('fas fa-filter') !!}
            {!! FluentForm::submit('reset', '')->css('btn btn-danger')->title(__('Resetuj'))->icon('fas fa-trash') !!}
        </div>
    </div>
</div>

{!! FluentForm::close() !!}
