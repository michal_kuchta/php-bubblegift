@extends('layouts.app')

@section('content')
    {!! $grid !!}

    {!! FluentForm::footer([
        Fluent::link(url($baseUrl.'/create'), __('Dodaj sposób płatności'))->css(['btn-primary'])
    ]) !!}
@endsection
