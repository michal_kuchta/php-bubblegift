@extends('layouts.app')

@section('content')
    <h1>{!! $model->id > 0 ? __('Edycja kategorii') : __('Tworzenie nowej kategorii') !!}</h1>

    @if (!empty($errors->all()))
        <div class="row">
            <div class="col-lg-12">
                <div class="card border-danger mb-3">
                    <div class="card-header">{!! __('Błędy formularza') !!}</div>
                    <div class="card-body text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

    {!! FluentForm::standard($model)->attr('autocomplete', 'off') !!}

    <div class="row">
        <div class="col-lg-6">
            {!! FluentForm::group()->text('name')->label(__('Nazwa kategorii')) !!}

            @if ($model->id > 0)
            {!! FluentForm::group()->text('slug', $model->slug)->label(__('Symbol kategorii'))->readonly()->disabled() !!}
            @endif
            {!! FluentForm::group()->select('parent_id', $categories)->label(__('Kategoria nadrzędna / Rodzic'))->placeholder(__('-- wybierz --')) !!}
        </div>
    </div>

    {!! FluentForm::footer([
        FluentForm::submit('save', __('Zapisz'))->css(['btn-success']),
        Fluent::link(url($baseUrl), __('Powrót'))->css(['btn-default'])
    ]) !!}
    {!! FluentForm::close() !!}
@endsection
