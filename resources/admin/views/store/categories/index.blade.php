@extends('layouts.app')

@section('content')
    {!! $grid !!}

    {!! FluentForm::footer([
        Fluent::link(url($baseUrl.'/create'), __('Dodaj kategorię'))->css(['btn-primary'])
    ]) !!}
@endsection
