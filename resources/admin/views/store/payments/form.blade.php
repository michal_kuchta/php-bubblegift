@extends('layouts.app')

@section('content')
    <h1>{!! $model->id > 0 ? __('Tworzenie nowego sposobu płatności') : __('Edycja sposobu płatności') !!}</h1>

    {!! FluentForm::standard($model)->attr('autocomplete', 'off') !!}

    <div class="row hasFluentFooter" id="vue-product-form">
        <div class="col-lg-6">
            {!! FluentForm::group()->text('name')->label(__('Nazwa')) !!}
            {!! FluentForm::group()->checkbox('is_active')->label(__('Aktywne'))->attr('v-model', 'is_active') !!}

            <div class="card text-dark bg-light mb-3">
                <div class="card-header">Ceny</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            {!! FluentForm::group()->text('price_net')->label(__('Cena netto'))->attr('v-model', 'price_net')->attr('v-on:keyup', 'changePriceNet') !!}
                        </div>
                        <div class="col-lg-4">
                            {!! FluentForm::group()->text('price_gross')->label(__('Cena brutto'))->attr('v-model', 'price_gross')->attr('v-on:keyup', 'changePriceGross') !!}
                        </div>
                        <div class="col-lg-4">
                            {!! FluentForm::group()->number('tax_rate', 23)->label(__('Wysokość stawki VAT'))->step(1)->min(1)->max(100)->attr('v-model', 'tax_rate')->attr('v-on:keyup', 'changeTaxRate') !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! FluentForm::group()->editor('description')->label(__('Opis')) !!}
        </div>
        @if (!empty($errors->all()))
            <div class="col-lg-3">
                <div class="card border-danger mb-3">
                    <div class="card-header">{!! __('Błędy formularza') !!}</div>
                    <div class="card-body text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>

    {!! FluentForm::footer([
        FluentForm::submit('save', __('Zapisz'))->css(['btn-success']),
        Fluent::link(url($baseUrl), __('Powrót'))->css(['btn-default'])
    ]) !!}
    {!! FluentForm::close() !!}

    <script>
        document.addEventListener("DOMContentLoaded", function(){
            new Vue({
                el: '#vue-product-form',
                data: {
                    price_net: {!! $model->price_net ?? 0 !!},
                    price_gross: {!! $model->price_net ?? 0 !!},
                    tax_rate: {!! $model->tax_rate ?? 23 !!},
                },
                mounted: function(){
                    this.price_gross = Math.round(this.price_net * ((100 + this.tax_rate) / 100));
                },
                methods: {
                    round: function(number)
                    {
                        return Math.round((number + Number.EPSILON) * 100) / 100;
                    },
                    changePriceNet: function (){
                        this.price_gross = this.round(parseFloat(this.price_net) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    },
                    changePriceGross: function (){
                        this.price_net = this.round(parseFloat(this.price_gross) / ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    },
                    changeTaxRate: function (){
                        this.price_gross = this.round(parseFloat(this.price_net) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    }
                }
            })
        });
    </script>
@endsection
