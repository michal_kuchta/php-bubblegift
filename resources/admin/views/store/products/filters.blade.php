{!! FluentForm::standard($model) !!}
<div class="p-2">
    <div class="row">
        <div class="col-lg-11">
            <div class="row">
                <div class="col-lg-3">
                    {!! FluentForm::group()->text('filters[name]')->placeholder(__('Nazwa')) !!}
                </div>
                <div class="col-lg-3">
                    {!! FluentForm::group()->text('filters[index]')->placeholder(__('Symbol')) !!}
                </div>
                <div class="col-lg-3">
                    {!! FluentForm::group()->select('filters[type]', [\App\Models\Store\Product::TYPE_PRODUCT => __('Produkt'), \App\Models\Store\Product::TYPE_ADDON => __('Dodatek')])->placeholder(__('Typ')) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-1 text-right">
            {!! FluentForm::submit('save', '')->css('btn btn-default')->title(__('Filtruj'))->icon('fas fa-filter') !!}
            {!! FluentForm::submit('reset', '')->css('btn btn-danger')->title(__('Resetuj'))->icon('fas fa-trash') !!}
        </div>
    </div>
</div>

{!! FluentForm::close() !!}
