@extends('layouts.app')

@section('content')
    <h1>{!! $model->id > 0 ? __('Edycja produktu') : __('Tworzenie nowego produktu') !!}</h1>

    @if (!empty($errors->all()))
        <div class="row">
            <div class="col-lg-12">
                <div class="card border-danger mb-3">
                    <div class="card-header">{!! __('Błędy formularza') !!}</div>
                    <div class="card-body text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

    {!! FluentForm::standard($model)->attr('autocomplete', 'off') !!}

    <div class="row hasFluentFooter" id="vue-product-form">
        <div class="col-lg-6">
            {!! FluentForm::group()->text('name')->label(__('Nazwa produktu'))->attr('v-model', 'name')->attr('v-on:keyup', 'slug') !!}
            {!! FluentForm::group()->text('index')->label(__('Symbol produktu'))->attr('v-model', 'index') !!}

            <div class="row">
                <div class="col-lg-6">
                    {!! FluentForm::group()->checkbox('is_active')->label(__('Produkt aktywny i widoczny na stronie'))->attr('v-model', 'is_active') !!}
                </div>
                <div class="col-lg-6">
                    {!! FluentForm::group()->select('type', [\App\Models\Store\Product::TYPE_PRODUCT => __('Produkt'), \App\Models\Store\Product::TYPE_ADDON => __('Dodatek')])->label(__('Typ'))->attr('v-model', 'type') !!}
                </div>
            </div>

            <div class="row" v-if="type == '{{\App\Models\Store\Product::TYPE_ADDON}}'">
                <div class="col-lg-6">
                    {!! FluentForm::group()->select('field_type', [
                        "text" => __('Tekst'),
                        "image" => __('Zdjęcie')
                    ])->placeholder(__('Brak'))->label(__('Typ pola')) !!}
                </div>
                <div class="col-lg-6">
                    {!! FluentForm::group()->text('field_title')->label(__('Nazwa pola')) !!}
                </div>
            </div>

            <div class="card text-dark bg-light mb-3">
                <div class="card-header">Ceny</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            {!! FluentForm::group()->text('price_net')->label(__('Cena netto'))->attr('v-model', 'price_net')->attr('v-on:keyup', 'changePriceNet') !!}
                        </div>
                        <div class="col-lg-4">
                            {!! FluentForm::group()->text('price_gross')->label(__('Cena brutto'))->attr('v-model', 'price_gross')->attr('v-on:keyup', 'changePriceGross') !!}
                        </div>
                        <div class="col-lg-4">
                            {!! FluentForm::group()->number('tax_rate', 23)->label(__('Wysokość stawki VAT'))->step(1)->min(1)->max(100)->attr('v-model', 'tax_rate')->attr('v-on:keyup', 'changeTaxRate') !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            {!! FluentForm::group()->number('discount_percentage')->label(__('Wysokość promocji w %'))->step(1)->min(0)->max(100)->attr('v-model', 'discount_percentage')->attr('v-on:keyup', 'changeDiscountPercentage') !!}
                        </div>
                        <div class="col-lg-4">
                            {!! FluentForm::group()->text('discount_price')->label(__('Cena promocyjna netto'))->attr('v-model', 'discount_price')->attr('v-on:keyup', 'changeDiscountPriceNet') !!}
                        </div>
                        <div class="col-lg-4">
                            {!! FluentForm::group()->text('discount_price_gross')->label(__('Cena promocyjna brutto'))->attr('v-model', 'discount_price_gross')->attr('v-on:keyup', 'changeDiscountPriceGross') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-3">
                <h5>{!! __('Zdjęcia') !!}</h5>
                <template v-if="photos.length > 0">
                    <div class="row">
                        <div class="col-4" v-for="(photo, photoIndex) in photos" :key="photoIndex">
                            <div v-if="photos[photoIndex].id > 0" class="text-center">
                                <img :src="photos[photoIndex].url" height="100">
                            </div>
                            <input
                                type="file"
                                class="pt-2"
                                :ref="'photo-' + photoIndex"
                                @change="(event) => {fileSelected(photoIndex, event)}"
                            >
                            <input type="hidden" name="images_list[]" :value="photos[photoIndex].id">
                            <button
                                v-if="photos[photoIndex].id > 0"
                                class="btn btn-danger mt-2 w-100"
                                @click.prevent="deletePhoto(photoIndex)"
                            >Usuń zdjęcie
                            </button>
                        </div>
                    </div>
                    <hr class="pb-2">
                </template>
                <button class="btn btn-primary" @click.prevent="addPhoto">Dodaj zdjęcie</button>
            </div>
        </div>
        <div class="col-lg-6">
            {!! FluentForm::group()->editor('description')->label(__('Opis')) !!}

            <div class="card text-dark bg-light mb-3">
                <div class="card-header">
                    {!! __('Kategorie') !!}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            {!! __('Wybrane kategorie') !!}<br>
                            <div v-for="(category, key) in categories" style="margin-bottom: 5px">
                                <input type="hidden" name="categories[]" :value="category.id">
                                <span style="width: calc(100% - 40px); display: inline-block">@{{ category.name }}</span>
                                <span class="btn btn-danger" v-on:click="removeCategory(key)">x</span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            {!! __('Wyszukaj kategorie') !!}<br>
                            {!! FluentForm::group()->text(null)->attr('v-model', 'categoryQuery')->placeholder(__('Wpisz aby wyszukać...')) !!}
                            <div v-for="(category, key) in foundCategories" style="margin-bottom: 5px">
                                <span style="width: calc(100% - 40px); display: inline-block">@{{ category.name }}</span>
                                <span class="btn btn-success" v-on:click="addCategory(category)">+</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card text-dark bg-light mb-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-lg-6">
                            {!! __('Tagi') !!}
                        </div>
                        <div class="col-lg-6 pull-right">
                            <span class="btn btn-success" v-on:click="addTag()" style="float: right">{!! __('Dodaj tag') !!}</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12" v-for="(tag, key) in tags">
                            <div class="form-group">
                                <input placeholder="Tag" type="text" name="tags[]" v-model="tags[key]" class="form-control" style="width: calc(100% - 40px); display: inline-block">
                                <span class="btn btn-danger" v-on:click="deleteTag(key)">X</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! FluentForm::footer([
        FluentForm::submit('save', __('Zapisz'))->css(['btn-success']),
        Fluent::link(url($baseUrl), __('Powrót'))->css(['btn-default'])
    ]) !!}
    {!! FluentForm::close() !!}

    <script>
        document.addEventListener("DOMContentLoaded", function(){
            new Vue({
                el: '#vue-product-form',
                data: {
                    name: '{!! $model->name !!}',
                    index: '{!! $model->index !!}',
                    price_net: {!! $model->price_net ?? 0 !!},
                    price_gross: {!! $model->price_net ?? 0 !!},
                    discount_percentage: {!! $model->discount_percentage ?? 0 !!},
                    discount_price: {!! $model->discount_price ?? 0 !!},
                    discount_price_gross: {!! $model->discount_price ?? 0 !!},
                    tax_rate: {!! $model->tax_rate ?? 23 !!},
                    is_active: {!! $model->is_active ? "true" : "false" !!},
                    photos: {!! json_encode($model->images()->toArray()) !!},
                    tags: {!! json_encode($model->getTags()) !!},
                    type: "{{ $model->type ?? \App\Models\Store\Product::TYPE_PRODUCT }}",
                    categories: {!! json_encode($model->categories->map(function ($category){ return ['id' => $category->id, 'name' => $category->name];})->toArray()) !!},
                    categoryQuery: '',
                    foundCategories: []
                },
                mounted: function(){
                    this.price_gross = Math.round(this.price_net * ((100 + this.tax_rate) / 100));
                    this.discount_price_gross = Math.round(this.discount_price * ((100 + this.tax_rate) / 100));
                },
                methods: {
                    slug: function (){
                        this.index = this.name.toLowerCase()
                            .replace(/ę/g,"e")
                            .replace(/ó/g,"o")
                            .replace(/ą/g,"a")
                            .replace(/ś/g,"s")
                            .replace(/ł/g,"l")
                            .replace(/ż/g,"z")
                            .replace(/ź/g,"z")
                            .replace(/ć/g,"c")
                            .replace(/ń/g,"n")
                            .replace(/ /g,'-')
                            .replace(/[^\w-]+/g,'');
                    },
                    round: function(number)
                    {
                        return Math.round((number + Number.EPSILON) * 100) / 100;
                    },
                    changeRate: function (){
                        this.price_gross = this.round(parseFloat(this.price_net) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                        this.discount_price_gross = this.round(this.discount_price * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    },
                    changePriceNet: function (){
                        this.price_gross = this.round(parseFloat(this.price_net) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    },
                    changePriceGross: function (){
                        this.price_net = this.round(parseFloat(this.price_gross) / ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    },
                    changeDiscountPriceNet: function (){
                        this.discount_price_gross = this.round(parseFloat(this.discount_price) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                        this.discount_percentage = 0;
                    },
                    changeDiscountPriceGross: function (){
                        this.discount_price = this.round(parseFloat(this.discount_price_gross) / ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                        this.discount_percentage = 0;
                    },
                    changeDiscountPercentage: function (){
                        this.discount_price = this.round(parseFloat(this.price_net) * ((100.0 - parseFloat(this.discount_percentage)) / 100.0));
                        this.discount_price_gross = this.round(parseFloat(this.discount_price) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    },
                    changeTaxRate: function (){
                        this.price_gross = this.round(parseFloat(this.price_net) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                        this.discount_price_gross = this.round(parseFloat(this.discount_price) * ((100.0 + parseFloat(this.tax_rate)) / 100.0));
                    },

                    addTag(){
                        this.tags.push('');
                    },
                    deleteTag(key){
                        this.tags.splice(key, 1);
                    },

                    addPhoto() {
                        this.photos.push({
                            id: 0,
                            id: 0,
                            url: ''
                        });
                    },
                    fileSelected(photoIndex, event) {
                        let formData = new FormData();
                        formData.append('test', 'test');
                        formData.append("photo", event.target.files[0]);

                        $.ajax({
                            // Your server script to process the upload
                            url: '{{ url(config('custom.admin.prefix').'/photos/store') }}',
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: (response) => {
                                this.photos[photoIndex].id = response.id;
                                this.photos[photoIndex].url = response.url;
                            }
                        });
                    },
                    deletePhoto(photoIndex) {
                        if (this.$refs['photo-' + photoIndex].length > 0) {
                            this.$refs['photo-' + photoIndex][0].value = null;
                        }

                        this.photos.splice(photoIndex, 1);
                    },

                    findCategories(){
                        if (this.categoryQuery.length == 0)
                        {
                            this.foundCategories = [];
                            return;
                        }

                        $.ajax({
                            // Your server script to process the upload
                            url: '{{ url(config('custom.admin.prefix').'/store/categories/search') }}',
                            type: 'POST',
                            data: {
                                query: this.categoryQuery,
                                selected: this.categories.map(function (category){
                                    return category.id;
                                })
                            },
                            success: (response) => {
                                this.foundCategories = response.categories;
                            }
                        });
                    },
                    removeCategory(key){
                        this.categories.splice(key, 1);
                        this.findCategories();
                    },
                    addCategory(category){
                        this.categories.push(category);
                        this.findCategories();
                    },
                },
                watch: {
                    categoryQuery: function (val){
                        this.findCategories();
                    }
                }
            })
        });
    </script>
@endsection
