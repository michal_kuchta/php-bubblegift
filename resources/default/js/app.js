require('./bootstrap');

jQuery(function (){
    $('.owl-carousel.banners').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        items: 1,
        dots: false,
        navText: ["<i class=\"fas fa-chevron-left\"></i>", "<i class=\"fas fa-chevron-right\"></i>"],
        autoplay: true
    })
    $('.owl-carousel.products').owlCarousel({
        loop:false,
        margin:30,
        nav:false,
        responsive: {
            0 : {
                items: 1
            },
            480 : {
                items: 3
            },
            768 : {
                items: 4
            }
        },
        dots: false,
        navText: ["<i class=\"fas fa-chevron-left\"></i>", "<i class=\"fas fa-chevron-right\"></i>"],
        autoplay: true
    });

    $('.owl-carousel.products').each(function(){
        var maxHeight = 0;

        $(this).find('.product-card').each(function(){
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $(this).find('.product-card').height(maxHeight);
    });

    $('.owl-carousel.product__image-thumbs').owlCarousel({
        loop:false,
        margin:30,
        nav:false,
        responsive: {
            0 : {
                items: 2
            },
            480 : {
                items: 2
            },
            768 : {
                items: 3
            }
        },
        dots: false,
        navText: ["", ""],
        autoplay: true
    })

    $('#flash-overlay-modal').modal();
});

