<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{!! __('Bubble Gift') !!}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="/default/css/vendor.css" rel="stylesheet">
    <link href="/default/css/app.css" rel="stylesheet">

    <script src="/default/js/vendor.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script src="/default/js/app.js"></script>
</head>
    <body class="antialiased">
        @include('layouts.partials.header')

        <main>
            <div class="container">
                @include('views.vendor.flash.message')
            </div>

            @yield('content')
        </main>

        @include('layouts.partials.footer')

        @stack('scripts')
    </body>
</html>
