<div class="main-header__authentication text-right" id="vue-authentication">
    <template v-if="!logged">
        <span class="main-header__authentication__button" v-on:click="showForm('login')">{!! __('Logowanie') !!}</span>
        <span class="main-header__authentication__button" v-on:click="showForm('register')">{!! __('Rejestracja') !!}</span>

        <div class="main-header__authentication__authenticate-box" v-if="formVisible != 'none'">
            <div class="main-header__authentication__authenticate-box__background" v-if="formVisible != 'none'" v-on:click="closeForm()"></div>

            <div class="main-header__authentication__authenticate-box__box" v-if="formVisible == 'login'">
                <span class="main-header__authentication__authenticate-box__close-button" v-on:click="closeForm()"><i class="fas fa-times"></i></span>
                <span class="main-header__authentication__authenticate-box__header d-block text-center">{!! __('Logowanie') !!}</span>

                <div class="main-header__authentication__authenticate-box__errors text-left" style="font-size: 14px;" v-if="loginErrors.length > 0">
                    <ul>
                        <li v-for="(error, key) in loginErrors" :key="key">@{{ error }}</li>
                    </ul>
                </div>

                {!! FluentForm::standard()->url(route('login'))->errors($errors)->css('no-submit-form') !!}

                {!! FluentForm::group()->email('email', old('email'))->placeholder(__('e-mail'))->attr('v-model', 'loginForm.email') !!}
                {!! FluentForm::group()->password('password')->placeholder(__('hasło'))->attr('v-model', 'loginForm.password') !!}

                <div class="text-center">
                    <span class="btn btn-primary cursor-pointer" v-on:click="login">{!! __('zaloguj') !!}</span>
                </div>

                <span class="d-block w-100 text-center">
                    {!! __('Nie masz jeszcze konta?') !!}
                </span>

                <span class="d-block w-100 text-center cursor-pointer" v-on:click="showForm('register')">
                    {!! __('Zarejestruj się.') !!}
                </span>

                {!! FluentForm::close() !!}
            </div>

            <div class="main-header__authentication__authenticate-box__box" v-if="formVisible == 'register'">
                <span class="main-header__authentication__authenticate-box__close-button" v-on:click="closeForm()"><i class="fas fa-times"></i></span>
                <span class="main-header__authentication__authenticate-box__header d-block text-center">{!! __('Rejestracja') !!}</span>

                <div class="main-header__authentication__authenticate-box__errors text-left" style="font-size: 14px;" v-if="registerErrors.length > 0">
                    <ul>
                        <li v-for="(error, key) in registerErrors" :key="key">@{{ error }}</li>
                    </ul>
                </div>

                {!! FluentForm::standard()->url(route('register'))->errors($errors)->css('no-submit-form') !!}

                {!! FluentForm::group()->text('first_name', old('first_name'))->placeholder(__('imię'))->attr('v-model', 'registerForm.first_name') !!}
                {!! FluentForm::group()->text('last_name', old('last_name'))->placeholder(__('nazwisko'))->attr('v-model', 'registerForm.last_name') !!}
                {!! FluentForm::group()->email('email', old('email'))->placeholder(__('e-mail'))->attr('v-model', 'registerForm.email') !!}

                {!! FluentForm::group()->password('password')->placeholder(__('hasło'))->attr('v-model', 'registerForm.password') !!}
                {!! FluentForm::group()->password('password_confirmation')->placeholder(__('powtórz hasło'))->attr('v-model', 'registerForm.password_confirmation') !!}

                <div class="text-center">
                    <span class="btn btn-primary cursor-pointer" v-on:click="register">{!! __('zarejestruj') !!}</span>
                </div>

                <span class="d-block w-100 text-center">
                    {!! __('Masz już konto?') !!}
                </span>
                <span class="d-block w-100 text-center cursor-pointer" v-on:click="showForm('login')">
                    {!! __('Zaloguj się.') !!}
                </span>

                {!! FluentForm::close() !!}
            </div>
        </div>
    </template>
    <template v-if="logged">
        {!! FluentForm::open()->route('logout') !!}
        <a href="#" class="main-header__authentication__button">{!! __('Moje konto') !!}</a>
        <span class="main-header__authentication__button logout-button">{!! __('Wyloguj') !!}</span>
        {!! FluentForm::close() !!}
    </template>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function(){
        $(document).on('click', '.logout-button', function (){
            $(this).closest('form').submit();
        });
        $(document).on('submit', '.no-submit-form', function (event){
            event.preventDefault();
        });
        new Vue({
            el: '#vue-authentication',
            data: {
                formVisible: 'none',
                logged: {!! auth()->check() ? 'true' : 'false' !!},
                hasErrors: {!! $errors->has('email') || $errors->has('password') ? 'true' : 'false' !!},
                loginErrors: [],
                loginForm: {
                    email: '',
                    password: ''
                },
                registerErrors: [],
                registerForm: {
                    first_name: '',
                    last_name: '',
                    email: '',
                    password: '',
                    password_confirmation: ''
                }
            },
            mounted() {
                if (!this.logged && this.hasErrors)
                    this.showForm('');
            },
            methods: {
                showForm: function (name){
                    this.closeForm();
                    this.formVisible = name;
                },
                closeForm: function (){
                    this.formVisible = 'none';

                    this.loginForm = {
                        email: '',
                        password: ''
                    };

                    this.registerForm = {
                        first_name: '',
                        last_name: '',
                        email: '',
                        password: '',
                        password_confirmation: ''
                    }
                },
                login: function (){
                    this.loginErrors = [];
                    axios.post('{{ route('login') }}', this.loginForm).then((response) => {
                        window.location.reload();
                    }).catch((response) => {
                        if (response.response.data.errors)
                        {
                            let errorsArray = [];
                            for(let key in response.response.data.errors)
                            {
                                for(let error in response.response.data.errors[key])
                                {
                                    errorsArray.push(response.response.data.errors[key][error]);
                                }
                            }
                            this.loginErrors = errorsArray;
                        }
                        else
                        {
                            this.loginErrors = ["{!! __('Podany login lub hasło są niepoprawne') !!}"];
                        }
                    });
                },
                register: function (){
                    this.registerErrors = [];
                    axios.post('{{ route('register') }}', this.registerForm).then((response) => {
                        window.location.reload();
                    }).catch((response) => {
                        if (response.response.data.errors)
                        {
                            let errorsArray = [];
                            for(let key in response.response.data.errors)
                            {
                                for(let error in response.response.data.errors[key])
                                {
                                    errorsArray.push(response.response.data.errors[key][error]);
                                }
                            }
                            this.registerErrors = errorsArray;
                        }
                        else
                        {
                            this.registerErrors = ["{!! __('Błąd przetwarzania formularza') !!}"];
                        }
                    });
                }
            }
        })
    });
</script>
