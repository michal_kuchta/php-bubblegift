<div class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 main-footer__menu-box">
                <span class="main-footer__menu-box__title">{!! __('Menu') !!}</span>
                <ul class="main-footer__menu-box__list">
                    <li>
                        <a href="#">{!! __('Produkty') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Spersonalizowane') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('O nas') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Usługi dodatkowe') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Realizacje') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Kontakt') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Pomoc') !!}</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 main-footer__join-box">
                <span class="main-footer__join-box__title">{!! __('Dołącz do nas') !!}</span>
                <div class="main-footer__join-box__socials">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
                <div class="main-footer__join-box__payments">
                    <span class="main-footer__join-box__payments__title">{!! __('Formy płatności') !!}</span>
                    <div class="main-footer__join-box__payments__box">
                        <img class="shadow2" src="/default/images/payment_placeholder.png">
                        <img class="shadow2" src="/default/images/payment_placeholder.png">
                        <img class="shadow2" src="/default/images/payment_placeholder.png">
                        <img class="shadow2" src="/default/images/payment_placeholder.png">
                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="main-footer__newsletter-box shadow2">
                    <span class="main-footer__newsletter-box__title">{!! __('Newsletter') !!}</span>
                    <span class="main-footer__newsletter-box__info">{!! __('Zapisz się do newsletter i bądź na bieżąco z naszą ofertą!') !!}</span>

                    {!! FluentForm::open()->errors($errors)->url('/newsletter/save') !!}

                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                            {!! FluentForm::group()->email('newsletter_email')->placeholder(__('e-mail')) !!}
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 text-right">
                            {!! FluentForm::submit('sign', __('Zapisz się'))->css('btn btn-primary') !!}
                        </div>
                    </div>

                    {!! FluentForm::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
