<header class="container main-header">
    @include('layouts.partials.authentication')

    <div class="main-header__logo text-center">
        <a href="/" title="{{ __('Strona główna') }}">
            <object data="/default/images/logo.svg"> </object>
        </a>
    </div>

    <div class="main-header__icons {{ request()->routeIs('store.catalog') ? 'withCategories' : '' }}">
        @if (request()->routeIs('store.catalog'))
        <div class="main-header__icons__categories">
            <span><i class="fas fa-bars"></i>{!! __('Kategorie') !!}</span>
            <div class="main-header__icons__categories__categories-background" style="display: none"></div>
            <div class="main-header__icons__categories__categories-box shadow2" style="display: none">
                <span class="main-header__icons__categories__categories-box__close">
                    <i class="fas fa-times"></i>
                </span>

                <h2 class="main-header__icons__categories__categories-box__header">{!! __('Kategorie') !!}</h2>
                <a href="{{ url("/katalog") }}" style="display: block;" class="{{ !$currentCategory ? 'active' : '' }}">
                    {!! __('wszystkie') !!}
                </a>
                @include('views.store.catalog.categories', ['categories' => $categories])
            </div>
        </div>
        @endif
        <div class="main-header__icons__menu">
            <i class="fas fa-bars"></i>
            <div class="main-header__icons__menu__menu-background" style="display: none"></div>
            <div class="main-header__icons__menu__menu-box shadow2" style="display: none">
                <span class="main-header__icons__menu__menu-box__close">
                    <i class="fas fa-times"></i>
                </span>

                <div class="main-header__icons__menu__menu-box__header">
                    {!! __('Poczta balonowa') !!}
                </div>
                <ul class="main-header__icons__menu__menu-box__list">
                    <li>
                        <a href="{{ url('/katalog') }}">{!! __('Produkty') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Spersonalizowane') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('O nas') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Usługi dodatkowe') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Realizacje') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Kontakt') !!}</a>
                    </li>
                    <li>
                        <a href="#">{!! __('Pomoc') !!}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
@if (isset($breadCrumbs) && !empty($breadCrumbs))
<section class="container">
    <div class="breadcrumbs">
        @foreach ($breadCrumbs as $crumb)
            <a href="{{ $crumb['url'] }}">
                {{ $crumb['name'] }}
                @if (!$loop->last)
                <i class="fas fa-chevron-right"></i>
                @endif
            </a>
        @endforeach
    </div>
</section>
@endif

@push ('scripts')
    <script>
        $('document').ready(function (){
            $('.main-header__icons__menu .fa-bars').on('click', function (e){
                e.preventDefault();

                $('.main-header__icons__menu__menu-background').fadeIn();
                $('.main-header__icons__menu__menu-box').fadeIn();
            });
            $('.main-header__icons__menu__menu-background').on('click', function (e){
                e.preventDefault();

                $('.main-header__icons__menu__menu-background').fadeOut();
                $('.main-header__icons__menu__menu-box').fadeOut();
            });
            $('.main-header__icons__menu__menu-box__close').on('click', function (e){
                e.preventDefault();

                $('.main-header__icons__menu__menu-background').fadeOut();
                $('.main-header__icons__menu__menu-box').fadeOut();
            });

            $('.main-header__icons__categories > span').on('click', function (e){
                e.preventDefault();

                $('.main-header__icons__categories__categories-background').fadeIn();
                $('.main-header__icons__categories__categories-box').fadeIn();
            });
            $('.main-header__icons__categories__categories-background').on('click', function (e){
                e.preventDefault();

                $('.main-header__icons__categories__categories-background').fadeOut();
                $('.main-header__icons__categories__categories-box').fadeOut();
            });
            $('.main-header__icons__categories__categories-box__close').on('click', function (e){
                e.preventDefault();

                $('.main-header__icons__categories__categories-background').fadeOut();
                $('.main-header__icons__categories__categories-box').fadeOut();
            });
        })
    </script>
@endpush
