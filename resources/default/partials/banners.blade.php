@if (isset($slides) && !empty($slides))
    <div class="container">
        <div class="owl-carousel owl-theme banners {{ isset($css) ? (is_array($css) ? implode(' ', $css) : $css) : '' }}" style="max-height: 300px; overflow: hidden; width: 100%;">
            @foreach ($slides as $slide)
                <img class="item w-100" src="{{ array_get($slide, 'src', '#') }}" alt="{{ array_get($slide, 'alt', '') }}" />
            @endforeach
        </div>
    </div>
@endif
