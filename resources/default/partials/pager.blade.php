@php
    $totalPages = ceil($total / $perPage);
    $url = request()->url();
@endphp
<div class="pager">
    @if ($pageIndex > 1)
        <a href="{{ $url }}">{!! __('Pierwsza') !!}</a>
    @endif
    @if ($pageIndex - 1 >= 1)
        <a href="{{ $url }}?pageIndex={{ $pageIndex - 1 }}">{!! __('Poprzednia') !!}</a>
    @endif
    @if ($pageIndex - 3 >= 1)
        <a href="{{ $url }}?pageIndex={{ $pageIndex - 3 }}">{{ $pageIndex - 3 }}</a>
    @endif
    @if ($pageIndex - 2 >= 1)
        <a href="{{ $url }}?pageIndex={{ $pageIndex - 2 }}">{{ $pageIndex - 2 }}</a>
    @endif
    @if ($pageIndex - 1 >= 1)
        <a href="{{ $url }}?pageIndex={{ $pageIndex - 1 }}">{{ $pageIndex - 1 }}</a>
    @endif
    <span>{{ $pageIndex }}</span>
    @if ($pageIndex + 1 <= $totalPages)
        <a href="{{ $url }}?pageIndex={{ $pageIndex + 1 }}">{{ $pageIndex + 1 }}</a>
    @endif
    @if ($pageIndex + 2 <= $totalPages)
        <a href="{{ $url }}?pageIndex={{ $pageIndex + 2 }}">{{ $pageIndex + 2 }}</a>
    @endif
    @if ($pageIndex + 3 <= $totalPages)
        <a href="{{ $url }}?pageIndex={{ $pageIndex + 3 }}">{{ $pageIndex + 3 }}</a>
    @endif
    @if ($pageIndex + 1 <= $totalPages)
        <a href="{{ $url }}?pageIndex={{ $pageIndex + 1 }}">{!! __('Następna') !!}</a>
    @endif
    @if ($totalPages > 1)
        <a href="{{ $url }}?pageIndex={{ $totalPages }}">{!! __('Ostatnia') !!}</a>
    @endif
</div>
