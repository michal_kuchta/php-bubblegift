<div class="{!! $class !!} product-card shadow2">
    <a href="{{ route('store.product', [str_slug($product->name) . ',' . $product->id]) }}">
        <i class="fas fa-heart product-card__favorite-icon cursor-pointer"></i>

        <div class="product-card__image">
            @if ($product->images()->count() > 0)
                <img src="{!! url($product->images()->first()->url) !!}" />
            @else
                <img class="product-card__image--placeholder" src="{!! url('default/images/placeholder.svg') !!}" />
            @endif
        </div>

        <div class="product-card__name">
            {!! $product->name !!}
        </div>

        <div class="product-card__price">
            {!! $product->getPrice() !!} {!! __('zł') !!}
        </div>
        <i class="fas fa-shopping-basket product-card__cart-icon cursor-pointer"></i>
    </a>
</div>
