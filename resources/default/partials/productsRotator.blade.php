@if (isset($products) && !empty($products))
    <div class="container owl-carousel owl-theme products {{ isset($css) ? (is_array($css) ? implode(' ', $css) : $css) : '' }}" style="max-height: 500px; overflow: hidden; width: 100%; {{ $styles ?? '' }}">
        @foreach ($products as $product)
            @include('partials.productCard', ['product' => $product, 'class' => 'item ' . $itemCss ?? ''])
        @endforeach
    </div>
@endif
