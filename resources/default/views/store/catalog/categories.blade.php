@if ($categories && $categories->count() > 0)
    @foreach ($categories as $category)
        @include('views.store.catalog.category', ['category' => $category])
    @endforeach
@endif
