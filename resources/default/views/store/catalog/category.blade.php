<a href="{{ url("/katalog/{$category->slug},{$category->id}") }}" style="display: block;" class="{{ $currentCategory && $category->id == $currentCategory->id ? 'active' : '' }}">
    {!! $category->name !!}
</a>

@if ($category->children && $category->children->count() > 0)
    <div class="subCategories" style="margin-left: {{ $category->level * 15 }}px">
        @include('views.store.catalog.categories', ['categories' => $category->children])
    </div>
@endif
