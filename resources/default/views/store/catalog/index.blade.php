@extends('layouts.app')

@section ('content')
    <div class="container">
        @if ($products->count() == 0)
            <h4 style="display: block; width: 100%; text-align: center">{!! __('Brak produktów do wyświetlenia') !!}</h4>
        @else
            <div class="products products-list row">
                @foreach ($products as $product)
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 product-row">
                        @include('partials.productCard', ['product' => $product, 'class' => ''])
                    </div>

                    @if ($loop->iteration % 4 == 0)
                        </div>
                        <div class="products products-list row">
                    @endif
                @endforeach
            </div>

            @include('partials.pager', ['total' => $totalItems, 'perPage' => $perPage, 'pageIndex' => $pageIndex])
        @endif
    </div>
@endsection
