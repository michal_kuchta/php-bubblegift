@extends('layouts.app')

@section ('content')
    <div class="container">
        <div class="row product" id="product-vue-element">
            <div class="col-lg-6 col-md-6 col-sm 12">
                <div class="product__image-box shadow2">
                    @if ($product->images()->count() == 0)
                        <img class="product__image-box--placeholder" src="{!! url('default/images/placeholder.svg') !!}" v-if="images.length == 0"/>
                    @else
                        <img class="product__image-box--image" src="{{ $product->images()->first()->url }}"/>
                    @endif
                </div>
                @if ($product->images()->count() > 1)
                    <div class="owl-carousel owl-theme product__image-thumbs">
                        @foreach ($product->images() as $image)
                            <div class="product__image-thumbs__thumb-box shadow2">
                                <img class="product__image-thumbs__thumb-box--image" src="{{ $image->url }}" />
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="col-lg-6 col-md-6 col-sm 12">
                <h1 class="product__title">{!! $product->name !!}</h1>

                <div class="product__addons shadow2" v-if="addons.length > 0">
                    <h2>{{ __('Wybierz dodatki') }}</h2>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-1" v-for="(addon, key) in addons" :key="key">
                            <div class="input-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" v-model="addon.selected">
                                        <span></span>
                                        @{{ addon.name }} (+@{{ addon.price }} zł)
                                    </label>
                                </div>

                                <textarea rows="3" class="form-control w-100" v-model="addon.field" v-if="addon.fieldType == 'text' && addon.selected" placeholder="{{ __('wpisz tekst...') }}"></textarea>
                                <span class="help" v-if="addon.fieldType == 'image' && addon.selected">{{ __('Zdjęcie prosimy przesłać w wiadomości email') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="product__price">
                    @if ($product->hasDiscount())
                        <span class="product__price--discounted">{{ formatPrice($product->getDiscountPrice()) }} {!! __('zł') !!}</span>
                    @endif
                    <span class="product__price--standard">{{ formatPrice($product->getPrice()) }} {!! __('zł') !!}</span>
                    <span class="product__price--addons" v-if="addonsTotalPrice > 0"> (+ @{{ addonsTotalPrice }}) {!! __('zł') !!}</span>
                </div>

                <div class="product__add-to-cart">
                    <button type="button" name="addToCart" class="btn btn-default addProductToCart shadow2">{{ __('Dodaj do koszyka') }}</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="product__description shadow2">{!! $product->description !!}</div>
            </div>
        </div>

        <div class="row" style="margin-top: 100px">
            <div class="col-lg-12 product__recommended_products">
                <h4 class="product__recommended_products__title w-100 text-center">{{ __('Mogą Ci się spodobać!') }}</h4>

                <div class="product__recommended_products__wrapper shadow2">
                    @include('partials.productsRotator', [
                        'products' => \App\Models\Store\Product::getRecommendedProducts(),
                        'itemCss' => 'white-template',
                        'styles' => 'margin-top: 20px;'
                    ])
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener("DOMContentLoaded", function(){
            new Vue({
                el: '#product-vue-element',
                data: {
                    hasDiscount: {{ $product->hasDiscount() ? 'true' : 'false' }},
                    discountPrice: "{{ formatPrice($product->getDiscountPrice()) }}",
                    price: "{{ formatPrice($product->getPrice()) }}",
                    addons: {!! json_encode($addons->map(function (\App\Models\Store\Product $addon) {
                        return [
                            'id' => $addon->id,
                            'name' => $addon->name,
                            'price' => formatPrice($addon->getPrice()),
                            'fieldType' => $addon->field_type,
                            'field' => '',
                            'selected' => false
                        ];
                    })) !!},
                    addonsTotalPrice: 0
                },
                watch: {
                    addons: {
                        handler: function () {
                            console.log('asfasfasf');
                            this.prices = [];
                            this.addonsTotalPrice = 0;
                            let sum = 0;

                            for (let addonKey in this.addons)
                            {
                                let addon = this.addons[addonKey];
                                console.log(addon);
                                if (addon.selected)
                                {
                                    sum += parseFloat(addon.price);
                                }
                            }

                            this.addonsTotalPrice = (Math.round(sum * 100) / 100);
                        },
                        deep: true
                    }
                }
            })

            $(document).on('click', '.product__image-thumbs__thumb-box', function (e){
                e.preventDefault();
                let el = $(this);

                $('.product__image-box--image').fadeOut(400, function (){
                    $('.product__image-box--image').attr('src', el.find('img').first().attr('src'));
                    $('.product__image-box--image').fadeIn();
                });
            })
        });
    </script>
@endsection

