@extends('layouts.app')

@section ('content')
    @include('partials.banners', [
        'slides' => [
            ['src' => url('/default/images/banners/banner1.jpg'), 'alt' => ''],
            ['src' => url('/default/images/banners/banner2.jpg'), 'alt' => ''],
            ['src' => url('/default/images/banners/banner3.jpg'), 'alt' => '']
        ],
        'css' => 'shadow2'
    ])

    @include('partials.productsRotator', [
        'products' => \App\Models\Store\Product::getProductsForTag('asd')
    ])

    <div class="welcome-middle-bar">
        <div class="container">
            <div class="row">
                <div class="col">
                    <span class="welcome-middle-bar__icon">
                        <i class="fas fa-map-marker-alt"></i>
                    </span>
                    <span class="welcome-middle-bar__text">{!! __('Wysyłka na terenie całej polski') !!}</span>
                </div>
                <div class="col">
                    <span class="welcome-middle-bar__icon">
                        <i class="fas fa-calendar-alt"></i>
                    </span>
                    <span class="welcome-middle-bar__text">{!! __('Sam wybierasz datę dostarczenia przesyłki') !!}</span>
                </div>
                <div class="col">
                    <span class="welcome-middle-bar__icon">
                        <img src="/default/images/balloon-icon.svg">
                    </span>
                    <span class="welcome-middle-bar__text">{!! __('Pełen profesjonalizm') !!}</span>
                </div>
                <div class="col">
                    <span class="welcome-middle-bar__icon">
                        <i class="fas fa-gifts"></i>
                    </span>
                    <span class="welcome-middle-bar__text">{!! __('Na każdą okazję') !!}</span>
                </div>
                <div class="col">
                    <span class="welcome-middle-bar__icon">
                        <i class="fas fa-file-signature"></i>
                    </span>
                    <span class="welcome-middle-bar__text">{!! __('Indywidualne podejście do klienta') !!}</span>
                </div>
            </div>
        </div>
    </div>

    @include('partials.productsRotator', [
        'products' => \App\Models\Store\Product::getProductsForTag('dsa')
    ])

    <div class="welcome-hr"></div>

    <div class="container welcome-contact">
        <div class="row">
            <div class="col-lg-6 col-sm-12 welcome-contact__form">
                <span class="welcome-contact__form__title">
                    {!! __('Masz pytania? <br>Skontaktuj się z nami.') !!}
                </span>
                <div class="welcome-contact__form__information">
                    <img src="/default/images/envelope.svg" class="welcome-contact__form__information__icon" />
                    <div class="welcome-contact__form__information__text">
                        <span>{!! __('e-mail') !!}</span>
                        {!! __('kontakt.bubblegift@gmail.com') !!}
                    </div>
                </div>
                <div class="welcome-contact__form__information">
                    <i class="fa fa-mobile-alt welcome-contact__form__information__icon"></i>
                    <div class="welcome-contact__form__information__text">
                        <span>{!! __('Zadzwoń!') !!}</span>
                        {!! __('000 000 000') !!}
                    </div>
                </div>
                {!! FluentForm::open()->errors($errors)->url('/contact/save') !!}

                {!! FluentForm::group()->text('name')->placeholder(__('imię')) !!}
                {!! FluentForm::group()->email('email')->placeholder(__('e-mail')) !!}
                {!! FluentForm::group()->text('phone')->placeholder(__('numer telefonu')) !!}
                {!! FluentForm::group()->textarea('message')->placeholder(__('wiadomość')) !!}
                {!! FluentForm::submit('send', __('Wyślij')) !!}

                {!! FluentForm::close() !!}
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="welcome-contact__information">
                    <span class="welcome-contact__form__title">
                        {!! __('Tu znajdziesz odpowiedzi na <br>najczęściej zadawane pytania.') !!}
                    </span>
                    <ul>
                        <li>{!! __('Jak długo unoszą się balony?') !!}</li>
                        <li>{!! __('Na jakim terenie wysyłamy balony?') !!}</li>
                        <li>{!! __('Czy można zwrócić zamówienie?') !!}</li>
                    </ul>
                    <a class="btn btn-primary" href="#">{!! __('Sprawdź') !!}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
