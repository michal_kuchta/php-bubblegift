<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Podany login lub hasło są niepoprawne',
    'password' => 'Podany login lub hasło są niepoprawne',
    'throttle' => 'Zbyt dużo prób logowania. Poczekaj :seconds sekund i spróbuj ponownie.',

];
