<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Pole :attribute musi zostać zaakceptowane.',
    'active_url' => 'Wartość pola :attribute nie jest prawidłowym adresem URL',
    'after' => 'Data z pola :attribute musi być datą późniejszą niż :date.',
    'after_or_equal' => 'Data z pola :attribute musi być datą późniejszą lub równą niż :date.',
    'alpha' => 'Pole :attribute może zawierać tylko litery.',
    'alpha_dash' => 'Pole :attribute może zawierać tylko litery, cyfry, znaki - oraz _.',
    'alpha_num' => 'Pole :attribute może zawierać tylko litery oraz cyfry.',
    'array' => 'Pole :attribute musi być tablicą.',
    'before' => 'Data z pola :attribute musi być datą wcześniejszą niż :date.',
    'before_or_equal' => 'Data z pola :attribute musi być datą wcześniejszą lub równą niż :date.',
    'between' => [
        'numeric' => 'Wartość pola :attribute musi być wartościa mieszącą się w przedziale od :min do :max.',
        'file' => 'Waga pliku :attribute musi mieścić się pomiędzy :min oraz :max kilobitów.',
        'string' => 'Ilość znaków pola :attribute musi mieścić się w przedziale pomiędzy :min oraz :max.',
        'array' => 'Pole :attribute musi mieć ilość elementów pomiędzy :min oraz :max.',
    ],
    'boolean' => 'Wartość pola :attribute może być podane jako prawda lub fałsz.',
    'confirmed' => 'Wartość pola :attribute oraz jego potwierdzenie nie są takie same.',
    'date' => 'Wartość pola :attribute nie jest poprawną datą.',
    'date_equals' => 'Wartość pola :attribute musi być równa dacie :date.',
    'date_format' => 'Wartość pola :attribute nie jest w formacie :format.',
    'different' => 'Wartośc pola :attribute oraz :other muszą być różne.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'Wartość pola :attribute musi być prawidłowym adresem e-mail.',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'exists' => 'Wartość pola :attribute jest nieprawidłowa.',
    'file' => 'Wartość pola :attribute musi być plikiem.',
    'filled' => 'Pole :attribute musi mieć podaną wartość.',
    'gt' => [
        'numeric' => 'Wartość pola :attribute musi być większa niż :value.',
        'file' => 'Wartość pola :attribute musi być większa niż :value kilobitów.',
        'string' => 'Wartość pola :attribute musi mieć więcej znaków niż :value.',
        'array' => 'Wartość pola :attribute musi mieć więcej elementów niż :value.',
    ],
    'gte' => [
        'numeric' => 'Wartość pola :attribute musi być większa bądź równa niż :value.',
        'file' => 'Wartość pola :attribute musi być większa bądź równa niż :value kilobytes.',
        'string' => 'Wartość pola :attribute musi mieć tyle samo lub więcej znaków niż :value.',
        'array' => 'Wartość pola :attribute musi mieć tyle samo lub więcej elementów niż :value.',
    ],
    'image' => 'Wartość pola :attribute musi być zdjęciem.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'Wartość pola :attribute musi być liczbą.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'Wartość pola :attribute nie może być większa od :max.',
        'file' => 'Plik w polu :attribute nie może ważyć więcej niż :max kilobitów.',
        'string' => 'Wartość pola :attribute nie może mieć więcej znaków niż :max.',
        'array' => 'Wartość pola :attribute nie może mieć więcej elementów niż :max.',
    ],
    'mimes' => 'Wartość pola :attribute musi być plikiem typu :values.',
    'mimetypes' => 'Wartość pola :attribute musi być plikiem typu :values.',
    'min' => [
        'numeric' => 'Minimalna wartość dla pola :attribute to :min.',
        'file' => 'Minimalna waga dla pliku z pola :attribute to :min kilobitów.',
        'string' => 'Minimalna długość tekstu w polu :attribute to :min znaków.',
        'array' => 'Minimalna ilość elementów w tablicy pola :attribute to :min.',
    ],
    'multiple_of' => 'The :attribute must be a multiple of :value.',
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'Wartość pola :attribute musi być liczbą.',
    'password' => 'Hasło lub login są nieprawidłowe.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'Pole :attribute jest wymagane.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'prohibited' => 'The :attribute field is prohibited.',
    'prohibited_if' => 'The :attribute field is prohibited when :other is :value.',
    'prohibited_unless' => 'The :attribute field is prohibited unless :other is in :values.',
    'same' => 'Wartości pól :attribute oraz :other muszą być takie same.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'Wartość pola :attribute musi być tekstem.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => ':attribute został już zajęty',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'email' => __('E-mail'),
        'password' => __('Hasło'),
        'password_confirmation' => __('Powtórz hasło'),
        'first_name' => __('Imię'),
        'last_name' => __('Nazwisko'),
    ],

];
