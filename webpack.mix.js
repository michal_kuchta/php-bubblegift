const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/admin/js/app.js', 'public/admin/js')
    .scripts([
        'node_modules/@fortawesome/fontawesome-free/js/brands.js',
        'node_modules/@fortawesome/fontawesome-free/js/solid.js',
        'node_modules/@fortawesome/fontawesome-free/js/fontawesome.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
        'node_modules/metismenu/dist/metisMenu.js',
    ], 'public/admin/js/vendor.js')
    .styles([
        'node_modules/metismenu/dist/metisMenu.css',
    ], 'public/admin/css/vendor.css')
    .sass('resources/admin/scss/app.scss', 'public/admin/css')
    .sourceMaps()
    .copy('resources/admin/js/tinymce', 'public/tinymce');

mix.js('resources/default/js/app.js', 'public/default/js')
    .scripts([
        'node_modules/@fortawesome/fontawesome-free/js/brands.js',
        'node_modules/@fortawesome/fontawesome-free/js/solid.js',
        'node_modules/@fortawesome/fontawesome-free/js/fontawesome.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
        'resources/default/js/owl.carousel.js',
    ], 'public/default/js/vendor.js')
    .styles([
        'resources/default/css/owl.carousel.css',
    ], 'public/default/css/vendor.css')
    .sass('resources/default/scss/app.scss', 'public/default/css')
    .sourceMaps()
    .copy('resources/default/images', 'public/default/images')
    .copy('resources/default/fonts', 'public/default/fonts');
